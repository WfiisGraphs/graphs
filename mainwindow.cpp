
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <math.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QMainWindow::showMaximized();

    //ui->tab->setStyleSheet("background:transparent");
    //ui->tab->setAttribute(Qt::WA_TranslucentBackground);

    //ui->tabs->setStyleSheet("background:transparent");
    ui->tabs->setAttribute(Qt::WA_TranslucentBackground);

    //QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
   // QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForLocale(QTextCodec::codecForName("UTF-8"));

    ActiveGraph = NULL;
    ActiveGraphMethod = NONE;
    EulerCycleActiveVertex = 0;
}

MainWindow::~MainWindow()
{
    if(ActiveGraph){
        delete ActiveGraph;
    }
    delete ui;
}

void MainWindow::on_actionTest1_triggered()
{
    ActiveGraphMethod = TEST1;

    repaint();

}
void MainWindow::paintEvent(QPaintEvent *event){

    QSize windowSize = MainWindow::size();

    if(ActiveGraphMethod == TEST1 ){



        QPainter painter(this);

        painter.setRenderHint(QPainter::Antialiasing, true);
        painter.setPen(QPen(Qt::black, 2, Qt::SolidLine, Qt::RoundCap));
        painter.setBrush(QBrush(Qt::green, Qt::SolidPattern));

        QLineF line(0, 0, 100.0, 100.0);
        painter.drawLine(line);
        painter.drawEllipse(QPointF(150,150), 50, 50);
        painter.drawText(QPoint(150, 150), "12");

    }
    else if(ActiveGraphMethod == DRAW_ACTIVE_GRAPH && ActiveGraph){



        bool showIndexes = this->ui->showIndexes->isChecked();
        bool showWeights = this->ui->showWeights->isChecked();

        QPainter painter(this);

        double r = ui->rGraphSlider->value();

        //qDebug(QString("r = ").append(QString::number(r)).toStdString().c_str());

        double a = (2 * M_PI ) / ActiveGraph->GetVerticeCount();

        double startX = windowSize.width() - (windowSize.width() / 3);
        double startY = windowSize.height() / 2.0;

        GraphDrawer drawer(&painter,
                           ActiveGraph,
                           r,
                           a,
                           startX,
                           startY,
                           showIndexes,
                           showWeights,
                           VertexIndexesToColor,
                           EdgesIndexesToColor,
                           EulerCycleActiveVertex);

        drawer.drawGraph();


    }

}

void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWindow::on_actionOpenAdjacencyMatrixFromFile_triggered()
{

    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Graph"), "/home/", tr("Text Files (*.txt)"));

    std::string stdFileName = fileName.toStdString();

    std::ifstream MatrixFile(stdFileName.c_str());


    try{
        CheckFile(MatrixFile);
        if(ActiveGraph != NULL){
            delete ActiveGraph;
        }
        ActiveGraph = new graph(ConvertToMatrix(MatrixFile,AdjacencyMatrix));

        MatrixFile.close();
    }
    catch(...){
        callErrorBox("Nie udało się wczytać grafu.");
        ActiveGraph = NULL;
    }

    ActiveGraphMethod = DRAW_ACTIVE_GRAPH;

    updateGraphStructure();
    ui->tabs->setCurrentIndex(0);
}

void MainWindow::on_rGraphSlider_sliderMoved(int position)
{
    repaint();
}

void MainWindow::on_actionOpenAdjacencyListFromFile_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Graph"), "/home/", tr("Text Files (*.txt)"));

    std::string stdFileName = fileName.toStdString();

    std::ifstream MatrixFile(stdFileName.c_str());

    try{
        CheckFile(MatrixFile);
        if(ActiveGraph != NULL){
            delete ActiveGraph;
        }
        ActiveGraph = new graph(ConvertToMatrix(MatrixFile,AdjacencyList));

        MatrixFile.close();
    }
    catch(...){
        callErrorBox("Nie udało się wczytać grafu.");
        ActiveGraph = NULL;
    }

    ActiveGraphMethod = DRAW_ACTIVE_GRAPH;

    updateGraphStructure();
    ui->tabs->setCurrentIndex(0);
}



void MainWindow::on_actionOpenIncidenceMatrixFromFile_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this,
        tr("Open Graph"), "/home/", tr("Text Files (*.txt)"));

    std::string stdFileName = fileName.toStdString();

    std::ifstream MatrixFile(stdFileName.c_str());

    try{

        CheckFile(MatrixFile);
        if(ActiveGraph != NULL){
            delete ActiveGraph;
        }
        ActiveGraph = new graph(ConvertToMatrix(MatrixFile,IncidenceMatrix));

        MatrixFile.close();
    }
    catch(...){
        callErrorBox("Nie udało się wczytać grafu.");
        ActiveGraph = NULL;
    }

    ActiveGraphMethod = DRAW_ACTIVE_GRAPH;

    updateGraphStructure();
    ui->tabs->setCurrentIndex(0);
}

void MainWindow::updateGraphStructure(){
    unsigned edgeCount = 0;
    unsigned verticleCount = 0;
    ui->plainTextEdit->clear();
    ui->textDataField->clear();
    EdgesIndexesToColor.clear();
    VertexIndexesToColor.clear();
    ui->checkBox_2->setChecked(false);
    ui->MinimumSpanningTree->setChecked(false);
    if(ActiveGraph){
        edgeCount = ActiveGraph->GetEdgeCount();
        verticleCount = ActiveGraph->GetVerticeCount();
    }

    ui->edgesNumber->setText(QString::number(edgeCount));
    ui->verticlesNumber->setText(QString::number(verticleCount));

    EulerCycleActiveVertex = 0;
    //ui->textDataField->set
}

void MainWindow::on_tabs_currentChanged(int index)
{

    if(index == 0 || index == 2 || index == 4){
        ActiveGraphMethod = DRAW_ACTIVE_GRAPH;
        ui->textDataField->setHidden(true);
    }
    else{
        ActiveGraphMethod = NONE;
        ui->textDataField->setHidden(false);
    }
    repaint();
}

void MainWindow::on_PrintIncidenceMatrix_clicked()
{
    if(ActiveGraph){
        ui->textDataField->setPlainText(QString(ActiveGraph->PrintIncidenceMatrix().c_str()));
    }
    else{
        callErrorBox("Brak wczytanego grafu.");
    }
}

void MainWindow::on_PrintAdjacencyMatrix_clicked()
{
    if(ActiveGraph){
        ui->textDataField->setPlainText(QString(ActiveGraph->PrintAdjacencyMatrix().c_str()));
    }
    else{
        callErrorBox("Brak wczytanego grafu.");
    }
}

void MainWindow::on_PrintAdjacencyList_clicked()
{
    if(ActiveGraph){
        ui->textDataField->setPlainText(QString(ActiveGraph->PrintAdjacencyList().c_str()));
    }
    else{
        callErrorBox("Brak wczytanego grafu.");
    }
}

void MainWindow::callErrorBox(QString str){
    QMessageBox msgBox;
    msgBox.setText(str);
    msgBox.exec();
}

void MainWindow::on_doRandomGraph_clicked()
{
    int edges = ui->randomGraphVertexesNumber->value();
    double probability = ui->ranomGraphProbabilityNumber->value();
    try{
        if(ActiveGraph){
            delete ActiveGraph;
        }
        ActiveGraph = new graph(MakeRandomIncidenceProbability(edges,probability));
        updateGraphStructure();
        //ui->tabs->setCurrentIndex(0);
    }
    catch(...){
        callErrorBox("Błąd tworzenia grafu.");
        ActiveGraph = NULL;
    }
    repaint();
}

void MainWindow::on_showIndexes_clicked()
{
    repaint();
}

void MainWindow::on_actionSave_triggered()
{

    if(ActiveGraph){
        QString fileName = QFileDialog::getSaveFileName(this,tr("Save File"),"/home/",tr("Text Files (*.txt)"));
        ActiveGraph->SaveGraph(fileName.toStdString());
    }
    else{
        callErrorBox("Brak grafu do zapisu");
    }

}

void MainWindow::on_actionNew_triggered()
{
    if(ActiveGraph){
        delete ActiveGraph;
        ActiveGraph = NULL;
        repaint();
        updateGraphStructure();
    }
}

void MainWindow::on_checkGraphFromString_clicked()
{
    std::string str = (ui->sequence->toPlainText()).toStdString();
    try{
        GraphicSequence::checkIfStringIsGraphicSequence(str);
    }
    catch(std::runtime_error er){
        callErrorBox(er.what());
    }

    ui->textDataField->setPlainText(QString(GraphicSequence::getDebug().c_str()));
}

void MainWindow::on_makeGraphFromString_clicked()
{
    std::string str = (ui->sequence->toPlainText()).toStdString();
    if(ActiveGraph != NULL){
        delete ActiveGraph;
    }
    try{
        ActiveGraph = GraphicSequence::makeGraphFromString(str);
        ui->tabs->setCurrentIndex(0);

    }
    catch(std::runtime_error& e){
        callErrorBox(e.what());
        ActiveGraph = NULL;
    }
    ui->textDataField->setPlainText(QString(GraphicSequence::getDebug().c_str()));
    repaint();
    updateGraphStructure();
}

void MainWindow::on_checkBox_2_clicked(bool checked)
{
    if(checked){
        if(ActiveGraph){
            ui->MinimumSpanningTree->setChecked(false);
            ui->plainTextEdit->clear();
            EdgesIndexesToColor.clear();
            VertexIndexesToColor.clear();
            std::vector<unsigned> BiggestConnectedComponent = ActiveGraph->BiggestStronglyConnectedComponent();
            VertexIndexesToColor = BiggestConnectedComponent;
            if(BiggestConnectedComponent.size()==ActiveGraph->GetVerticeCount()){
                ui->plainTextEdit->setPlainText(QString("Graf jest silnie spojny."));
                for(unsigned EdgeIndex=1;EdgeIndex<=ActiveGraph->GetEdgeCount();++EdgeIndex) EdgesIndexesToColor.push_back(EdgeIndex);
            }
            else{
                QString Output("Najwieksza spojna skladowa w grafie:\n");
                std::vector<unsigned>::iterator it=BiggestConnectedComponent.begin();
                for(;it!=BiggestConnectedComponent.end();++it){
                    Output+=QString::number(*it);
                    Output+=", ";
                }
                std::vector<unsigned>::const_iterator itCol = BiggestConnectedComponent.begin();
                const std::vector<edge*>EdgesToColor =  ActiveGraph->GetEdges();
                std::vector<edge*>::const_iterator ite=EdgesToColor.begin();
                for(;ite!=EdgesToColor.end();++ite){
                    for(;itCol!=BiggestConnectedComponent.end();++itCol){
                        std::vector<unsigned>::const_iterator tempIt=itCol;
                        if((*ite)->CheckIfIncludesVertex(*itCol)){
                            itCol++;
                            for (;itCol!=BiggestConnectedComponent.end();++itCol){
                                if((*ite)->CheckIfIncludesVertex(*itCol))
                                    EdgesIndexesToColor.push_back((*ite)->getIndex());
                            }
                            itCol=tempIt;
                        }
                    }
                    itCol=BiggestConnectedComponent.begin();
                }
                ui->plainTextEdit->setPlainText(Output);
            }
        }
        else{
            callErrorBox("Brak wczytanego grafu.");
            ui->checkBox_2->setChecked(false);
        }
    }
    else {
        ui->plainTextEdit->clear();
        EdgesIndexesToColor.clear();
        VertexIndexesToColor.clear();
    }
    repaint();
}

void MainWindow::on_pushButton_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu");
        return;
    }
    try{
        std::vector<unsigned> HamiltonianCycle= ActiveGraph->FindHamiltonianCycle();
        EulerCycle = HamiltonianCycle;//funny
        if(!HamiltonianCycle.size()) ui->plainTextEdit->setPlainText(QString("Graf nie posiada cyklu Hamiltona."));
        else{
            QString Output("Cykl Hamiltona:\n");
            std::vector<unsigned>::iterator it=HamiltonianCycle.begin();
            for(;it!=HamiltonianCycle.end();++it){
                Output+=QString::number(*it);
                Output+=" -> ";
            }
            ui->plainTextEdit->setPlainText(Output);
        }
    }catch(std::runtime_error er){
        //callErrorBox(er.what());
        ui->plainTextEdit->setPlainText(er.what());
    }
}

void MainWindow::on_checkEulerCycle_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu");
        return;
    }

    try{
        std::vector<unsigned> cycle = ActiveGraph->FindEulerCycle();
        EulerCycle = cycle;
        QString str("Cykl Eulera:\n");
        for(std::vector<unsigned>::iterator it = cycle.begin(); it < cycle.end();it++){
            str += QString::number(*it);
            str += " -> ";
        }
        ui->plainTextEdit->setPlainText(str);

    }catch(std::runtime_error er){
        //callErrorBox(er.what());
        ui->plainTextEdit->setPlainText(er.what());
    }
    return;
}

void MainWindow::on_doRandomGraph_2_clicked()
{
    int vertex = ui->randomGraphVertexesNumber->value();
    int edges = ui->randomGraphEdgesNumber->value();
    try{
        if(ActiveGraph){
            delete ActiveGraph;
        }
        ActiveGraph = new graph(MakeRandomIncidence(vertex,edges));
        updateGraphStructure();
        //ui->tabs->setCurrentIndex(0);
    }
    catch(std::runtime_error er){
        callErrorBox(er.what());
        ActiveGraph =NULL;
    }
    repaint();
}


void MainWindow::on_randomSequenceGraph_clicked()
{
    std::string str = (ui->sequence->toPlainText()).toStdString();
    if(ActiveGraph != NULL){
        delete ActiveGraph;
    }
    try{
        ActiveGraph = RandomSequenceGraph(str);
        ui->tabs->setCurrentIndex(0);

    }
    catch(std::runtime_error& e){
        callErrorBox(e.what());
        delete ActiveGraph;
        ActiveGraph = NULL;
    }
    repaint();
    updateGraphStructure();
}


void MainWindow::on_doRandomEulerGraph_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak bazowego grafu");
        return;
    }

    try{
        ActiveGraph->BuildEulerGraph();
        //ui->tabs->setCurrentIndex(0);
    }catch(std::runtime_error er){
        if(ActiveGraph){
            delete ActiveGraph;
            ActiveGraph = NULL;
        }
        callErrorBox(er.what());
    }
    repaint();
}


void MainWindow::on_randomRagularGraph_clicked()
{
    unsigned k = ui->randomRegularGraphAdjacencyCount->value();
    try{
        if(ActiveGraph){
            delete ActiveGraph;
        }
        ActiveGraph = RandRegularGraph(k);
        updateGraphStructure();
        //ui->tabs->setCurrentIndex(0);
    }
    catch(std::runtime_error er){
        callErrorBox(er.what());
        ActiveGraph = NULL;
    }
    repaint();
}

void MainWindow::on_showWeights_clicked()
{
    repaint();
}

void MainWindow::on_doDijstraAlgorithm_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu.");
        return;
    }
    unsigned index = ui->dijstraStartIndex->value();
    try{
        dijkstraResult* result = ActiveGraph->DijkstraAlgorithm(index);
        QString s = "Rezultat algorytmu Dijstry\n";
        s.append("Odleglosci:\n");
        for(int i = 1; i < result->size; i++){
            s.append("[").append(QString::number(i)).append("] ").append(QString::number(result->distance[i])).append("\n");
        }
        s.append("Poprzednik:\n");
        for(int i = 1; i < result->size; i++){
            s.append("[").append(QString::number(i)).append("] ").append(QString::number(result->predecessorVertexIndex[i])).append("\n");
        }
        s.append("Najkrutsze sciezki:\n");
        for(int i = 1; i < result->size; i++){
                int j = result->predecessorVertexIndex[i];
                s.append("[").append(QString::number(i)).append("]: ").append(QString::number(i)).append(" ->");
                while(j != -1){
                    s.append(QString::number(j)).append(" ->");
                    j = result->predecessorVertexIndex[j];
                }
                s.append("\n");
        }
        ui->algorithmsTextField->setPlainText(s);
        delete result;
    }
    catch(std::runtime_error er){
        callErrorBox(er.what());
    }
}
void MainWindow::on_MinimumSpanningTree_clicked(bool checked)
{
    if(checked){
        if(ActiveGraph == NULL){
            callErrorBox("Brak wczytanego grafu");
            ui->MinimumSpanningTree->setChecked(false);
            return;
        }
        try{
            ui->checkBox_2->setChecked(false);
            ui->plainTextEdit->clear();
            EdgesIndexesToColor.clear();
            VertexIndexesToColor.clear();
            std::vector<unsigned> MinimumSpanningTree = ActiveGraph->FindMinimumSpanningTree();
            EdgesIndexesToColor=MinimumSpanningTree;
            std::vector<unsigned>::const_iterator it=MinimumSpanningTree.begin();
            QString str("Minimalne drzewo rozpinające:\n");
            for(; it < MinimumSpanningTree.end();it++){
                str += QString::number(*it);
                str += " -> ";
            }
            ui->plainTextEdit->setPlainText(str);
        }catch(std::runtime_error er){
            ui->plainTextEdit->setPlainText(er.what());
        }
    }
    else{
        ui->plainTextEdit->clear();
        EdgesIndexesToColor.clear();
        VertexIndexesToColor.clear();
    }
    repaint();
}

void MainWindow::on_randomConnectedComponent_clicked()
{
    try{
        if(ActiveGraph){
            delete ActiveGraph;
        }
        if(ui->randomGraphVertexesNumber->value()<2){
            callErrorBox("Minimalna ilość wierzchołków to 2.");
            return;
        }
        ActiveGraph = RandomConnectedComponent();
        updateGraphStructure();
        ui->tabs->setCurrentIndex(0);
    }
    catch(std::runtime_error er){
        callErrorBox(er.what());
        ActiveGraph = NULL;
    }
}

void MainWindow::on_printBiggestConnected_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu.");
        return;
    }
    try{
        graph* GRAPH=new graph(ActiveGraph->CreateBiggestConnectedComponent());
        delete ActiveGraph;
        ActiveGraph=GRAPH;
        updateGraphStructure();
        ui->tabs->setCurrentIndex(0);
        repaint();
    }
    catch(std::runtime_error er){
        callErrorBox(er.what());
        ActiveGraph = NULL;
    }

}

void MainWindow::on_matrixOfDistance_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu.");
        return;
    }
    try{
        TypeMatrixInt* Matrix=ActiveGraph->MatrixOfDistance();
        QString s = "Macierz odległości:\n";
        for(unsigned i=0;i<ActiveGraph->GetVerticeCount();i++){
            for(unsigned j=0;j<ActiveGraph->GetVerticeCount();j++){
                s.append(QString::number((*Matrix)[i][j])).append("  ");
                if((*Matrix)[i][j]<10)s.append("  ");
            }
            s.append("\n");
        }
        s.append("\n");
        unsigned Center=ActiveGraph->Center();
        s.append("Centrum grafu: \n");
        s.append(QString::number(Center)).append("\n");
        unsigned CenterMinMax=ActiveGraph->CenterMinMax();
        s.append("Centrum Min-Max: \n");
        s.append(QString::number(CenterMinMax)).append("\n");
        ui->algorithmsTextField->setPlainText(s);
        delete Matrix;
    }catch(std::runtime_error er){
        callErrorBox(er.what());
    }
}


void MainWindow::on_showCycleStep_clicked()
{
    if(!EulerCycle.empty()){
            EulerCycleActiveVertex = EulerCycle.at(0);
            VertexIndexesToColor.push_back(EulerCycle.at(0));
            EulerCycle.erase(EulerCycle.begin());
            repaint();
    }
    else{
        EulerCycle.clear();
        callErrorBox("Koniec cyklu.");
    }
}

void MainWindow::on_RandomDirected_clicked()
{
    try{
        if(ActiveGraph){
            delete ActiveGraph;
        }
        ActiveGraph = RandomConnectedComponent();
        updateGraphStructure();
        ui->tabs->setCurrentIndex(0);
    }
    catch(std::runtime_error er){
        callErrorBox(er.what());
        ActiveGraph = NULL;
    }
}

void MainWindow::on_SSS_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu");
        return;
    }
    try{
        std::vector< std::vector<unsigned> > StronglyConnectedComponents = ActiveGraph->StronglyConnectedComponent();
        QString Output("Silnie spójne Składowe:\n");
        std::vector< std::vector<unsigned> >::iterator it=StronglyConnectedComponents.begin();
        for(;it!=StronglyConnectedComponents.end();++it){
            std::vector<unsigned>::iterator it2=(*it).begin();
            for(;it2!=(*it).end();++it2){
                Output+=QString::number(*it2);
                Output+=" -> ";
            }
            Output+="\n";
        }


        Output+="\n";
        ui->plainTextEdit->setPlainText(Output);
    }catch(std::runtime_error er){
        //callErrorBox(er.what());
        ui->plainTextEdit->setPlainText(er.what());
    }
}

void MainWindow::on_Cycles_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu");
        return;
    }
    try{
        std::vector< std::vector<unsigned> > Cycles = ActiveGraph->Cycles();
        if(Cycles.size()==0){
            ui->plainTextEdit->setPlainText("Graf nie posiada żadnego cyklu.\n");
            return;
        }
        QString Output("Graf posiada: ");
        Output+= QString::number(Cycles.size());
        Output+=" Cykli:\n";
        std::vector< std::vector<unsigned> >::iterator it=Cycles.begin();
        for(;it!=Cycles.end();++it){
            std::vector<unsigned>::iterator it2=(*it).begin();
            for(;it2!=(*it).end();++it2){
                Output+=QString::number(*it2);
                Output+=" -> ";
            }
            Output+="\n";
        }
        Output+="\n";
        ui->plainTextEdit->setPlainText(Output);
    }catch(std::runtime_error er){
        //callErrorBox(er.what());
        ui->plainTextEdit->setPlainText(er.what());
    }
}

void MainWindow::on_doRandomDigraphACycle_clicked()
{
    int n = ui->randomGraphVertexesNumber->value();
    int p = ui->ranomGraphProbabilityNumber->value() * 100;

    ActiveGraph = RandomAcycle(n,p);
    updateGraphStructure();
    repaint();
}

void MainWindow::on_topologicalSort_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu");
        return;
    }
    this->ui->algorithmsTextField->setPlainText( ActiveGraph->TopologicSort() );
}

void MainWindow::on_doJohnsonAlgorithm_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu");
        return;
    }
    try{
        this->ui->algorithmsTextField->setPlainText( ActiveGraph->JohnsonAlgorithm() );
    }catch(std::runtime_error er){
        //callErrorBox(er.what());
        ui->algorithmsTextField->setPlainText(er.what());
    }
}

void MainWindow::on_BellmanFordAlgorithm_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu");
        return;
    }
    try{
        unsigned source=ui->dijstraStartIndex->value();
        if(source>ActiveGraph->GetVerticeCount())
            throw std::runtime_error("Nie istnieje wierzchołek o takim indeksie");
        std::vector<int> Distance = ActiveGraph->BellmanFordAlgorithm(source);
        QString Output("Odległości poszczególnych wierzchołków od wierzchołka o indeksie ");
        Output+= QString::number(source);
        Output+="\n";
        for(unsigned it=0;it<Distance.size();++it){
            Output+=QString::number(it+1);
            Output+=" -> ";
            if(Distance[it]>=100000000)
                Output+="ścieżka nie istnieje";
            else
                Output+=QString::number(Distance[it]);
            Output+="\n";
        }
        Output+="\n";
        ui->algorithmsTextField->setPlainText(Output);
    }catch(std::runtime_error er){
        //callErrorBox(er.what());
        ui->algorithmsTextField->setPlainText(er.what());
    }
}

void MainWindow::on_FloydAlgorithm_clicked()
{
    if(ActiveGraph == NULL){
        callErrorBox("Brak wczytanego grafu");
        return;
    }
    try{
     this->ui->algorithmsTextField->setPlainText( ActiveGraph->FloydWarshallAlgorithm());
    }catch(std::runtime_error er){
        //callErrorBox(er.what());
        ui->algorithmsTextField->setPlainText(er.what());
    }
}
