#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include <QLabel>
#include <QPicture>
#include <QPainter>

#include <QFileDialog>
#include <QMessageBox>
#include <QtCore/qmath.h>
#include <QTextCodec>

#include <fstream>
#include <string>
#include <sstream>

#include "GraphClasses/Graph.h"
#include "GraphClasses/EnumTypeOfRepresentation.h"
#include "GraphClasses/FileOperations.h"
#include "GraphClasses/Matrix.h"

#include "graphdrawer.h"

#include "GraphClasses/GraphicSequence.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    enum GraphMethod{
        NONE,
        DRAW_ACTIVE_GRAPH,
        TEST1
    };

    GraphMethod ActiveGraphMethod;
    graph* ActiveGraph;
    std::vector<unsigned> VertexIndexesToColor;
    std::vector<unsigned> EdgesIndexesToColor;

    std::vector<unsigned> EulerCycle;
    unsigned EulerCycleActiveVertex;

    void paintEvent(QPaintEvent *event);
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void updateGraphStructure();
    void callErrorBox(QString str);

private slots:
    void on_actionTest1_triggered();

    void on_actionExit_triggered();

    void on_actionOpenAdjacencyMatrixFromFile_triggered();

    void on_rGraphSlider_sliderMoved(int position);

    void on_actionOpenAdjacencyListFromFile_triggered();

    void on_actionOpenIncidenceMatrixFromFile_triggered();

    void on_tabs_currentChanged(int index);

    void on_PrintIncidenceMatrix_clicked();

    void on_PrintAdjacencyMatrix_clicked();

    void on_PrintAdjacencyList_clicked();

    void on_doRandomGraph_clicked();

    void on_showIndexes_clicked();

    void on_actionSave_triggered();

    void on_actionNew_triggered();

    void on_checkGraphFromString_clicked();

    void on_makeGraphFromString_clicked();

    void on_checkBox_2_clicked(bool checked);

    void on_pushButton_clicked();

    void on_checkEulerCycle_clicked();

    void on_doRandomGraph_2_clicked();

    void on_randomSequenceGraph_clicked();


    void on_doRandomEulerGraph_clicked();

    void on_randomRagularGraph_clicked();

    void on_MinimumSpanningTree_clicked(bool checked);

    void on_showWeights_clicked();

    void on_doDijstraAlgorithm_clicked();

    void on_randomConnectedComponent_clicked();

    void on_printBiggestConnected_clicked();

    void on_matrixOfDistance_clicked();


    void on_showCycleStep_clicked();

    void on_RandomDirected_clicked();

    void on_SSS_clicked();

    void on_Cycles_clicked();
    void on_doRandomDigraphACycle_clicked();

    void on_topologicalSort_clicked();

    void on_doJohnsonAlgorithm_clicked();

    void on_BellmanFordAlgorithm_clicked();

    void on_FloydAlgorithm_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
