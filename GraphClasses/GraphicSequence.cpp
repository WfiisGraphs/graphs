#include "GraphicSequence.h"

std::string GraphicSequence::debug = " ";


namespace Helper{

    std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
        std::stringstream ss(s);
        std::string item;
        while (std::getline(ss, item, delim)) {
            elems.push_back(item);
        }
        return elems;
    }


    std::vector<std::string> split(const std::string &s, char delim) {
        std::vector<std::string> elems;
        split(s, delim, elems);
        return elems;
    }

    bool PairIntWrapperComperator(const PairIntWrapper& l, const PairIntWrapper& r){
        return l.value > r.value;
    }

}


bool GraphicSequence::checkIfStringIsGraphicSequence(std::string string)
{
    bool isGraphicSequence = false;

    std::ostringstream ss;

    std::vector<std::string> v = Helper::split(string,',');
    std::vector<int> vn;
    int n = v.size();
    for(std::vector<std::string>::iterator it = v.begin(); it < v.end();it++){
        int temp = atoi((*it).c_str());
        if(temp > n){
            ss << "Sekwencja nie jest graficzna.";
            GraphicSequence::debug = ss.str();
            return false;
        }
        vn.push_back(temp);
    }
    for(std::vector<int>::iterator it2 = vn.begin(); it2 < vn.end();it2++){
       ss << (*it2) << " ";
    }
    ss << "\n";
    bool loop = true;
    while(loop){

        std::sort(vn.begin(), vn.end(), std::greater<int>());

        for(std::vector<int>::iterator it2 = vn.begin(); it2 < vn.end();it2++){
            ss << (*it2) << " ";
        }
        ss << "\n";
        std::vector<int>::iterator it = vn.begin();
        int sub = (*it);
        if(sub <= 0 || sub > n){
            loop = false;
            isGraphicSequence = false;
        }
        int i = 0;
        int sum = 0;
        for(std::vector<int>::iterator it2 = vn.begin(); it2 < vn.end();it2++){
            if(i != 0){
                if(sub > 0){
                    vn.at(i)--;
                    sub--;
                }
            }
            else{
                vn.at(i) = 0;
            }
            ss << (*it2) << " ";
            sum += abs(vn.at(i));
            i++;
        }
        ss << "\n";
        if(sum == 0){
            isGraphicSequence = true;
            loop = false;
        }
    }

    if(isGraphicSequence){
        ss << "\nSekwencja jest graficzna.";
    }
    else{
        ss << "\nSekwencja nie jest graficzna.";
    }

    GraphicSequence::debug = ss.str();

    return isGraphicSequence;
}

graph* GraphicSequence::makeGraphFromString(std::string string){

    std::ostringstream ss;

    if(!GraphicSequence::checkIfStringIsGraphicSequence(string)){
        GraphicSequence::debug = ss.str();
        throw std::runtime_error("Sekwencja nie jest graficzna");
    }

    std::vector<std::string> v = Helper::split(string,',');
    std::vector<PairIntWrapper> vn;
    int i = 0;
    for(std::vector<std::string>::iterator it = v.begin(); it < v.end();it++){
        vn.push_back( PairIntWrapper(i,atoi((*it).c_str())) );
        i++;
    }

    int n = vn.size();

    bool** g = new bool*[n];
    for(int i = 0; i < n; i++){
        g[i] = new bool[n];
        for(int j = 0; j < n; j++){
            g[i][j] = false;
        }
    }

    /*for(int i = 0; i < vn.size(); i++){
        for(int j = 0; j < vn.size(); j++){
            if(i != j && vn.at(i) > 0 && vn.at(j) > 0){
                if(g[i][j] == false){
                    g[i][j] = true;
                    g[j][i] = true;
                    vn.at(j)--;
                    vn.at(i)--;
                }
            }
        }
    }*/
    while(true){

        std::sort(vn.begin(), vn.end(), Helper::PairIntWrapperComperator);
        int sub = vn.at(0).value;
        int id = vn.at(0).id;
        vn.at(0).value = 0;
        if(sub <= 0){
            break;
        }

        for(unsigned i = 1; i < vn.size();i++){
            if(sub>0){
                g[id][vn.at(i).id] = true;
                g[vn.at(i).id][id] = true;
                vn.at(i).value--;
                sub--;
            }

        }
    }





    for(int i = 0; i < n; i++){
        for(int j = 0; j < n; j++){
            ss << g[i][j] << " ";
        }
        ss << "\n";
    }

    int sum = 0;
    ss << "\n[";
    for(std::vector<PairIntWrapper>::iterator it = vn.begin(); it < vn.end();it++){
        sum += (*it).value;
        ss << (*it).id << ":" << (*it).value << " ";
    }
    ss << "]\n";
    if(sum != 0){
        GraphicSequence::debug = ss.str();
        throw std::runtime_error("Nie udalo sis poprawnie stworzyc grafu.");
    }



    std::vector< std::vector<bool> >*  vg = new std::vector< std::vector<bool> >();

    for(int i = 0; i < n; i++){
        std::vector<bool> temp;
        for(int j = 0; j < n; j++){
            temp.push_back(g[i][j]);
        }
        vg->push_back(temp);
    }

    graph* newGraph = new graph(std::pair<int,TypeMatrix*>(AdjacencyMatrix,vg));

    for(int i = 0; i < n; i++){
        delete [] g[i];
    }
    delete [] g;

    GraphicSequence::debug = ss.str();

    return newGraph;
}
