#include "Vertex.h"
#include <stdexcept>
vertex::vertex(unsigned vertexindex):Colored(false),Index(vertexindex){}
vertex& vertex::AddVertex(vertex*& othervertex){
  if(!othervertex) throw std::runtime_error("Null pointer in vertex::AddVertex\terminated.\n");
  ConnectedVertices.insert(std::pair<unsigned,vertex*>(othervertex->Index,othervertex));
  return *this;
}
std::string vertex::PrintConnectedVertices()const{
  std::stringstream STREAM;
  STREAM << Index << " is connented to: " ;
  std::map<unsigned,vertex*>::const_iterator it =ConnectedVertices.begin();
  for(;it!=ConnectedVertices.end();++it)
    STREAM << *((*it).second) << ',';
  STREAM<<'\n';
  return STREAM.str();
}
