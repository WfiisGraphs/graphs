#include "Graph.h"
#include <fstream>
#include <sstream>
#include "EnumTypeOfRepresentation.h"
#include <QThread>
#include "Random.h"
#include "TopologicSort.h"
#include <math.h>
void graph::InitiateByAdjacencyMatrix(TypeMatrix* Matrix){
  for(unsigned it=1;it<=VerticeCount;it++)
  Vertices.insert(std::pair<unsigned,vertex*>(it,new vertex(it)));
  unsigned EdgeIndex=1;
  unsigned VetrexIndex=1;
  for(unsigned it=0;it<Matrix->size();++it){
    for(unsigned it2=0;it2<(*Matrix)[it].size();++it2)
      if((*Matrix)[it][it2]){
        Edges.push_back(new edge(EdgeIndex,Vertices[VetrexIndex],Vertices[it2+1]));
        Vertices[VetrexIndex]->AddVertex(Vertices[it2+1]);
        EdgeIndex++;
      }
      VetrexIndex++;
  }
  EdgeCount=Edges.size();
}

void graph::InitiateByIncidenceMatrix(TypeMatrix* Matrix){
  for(unsigned it=1;it<=VerticeCount;it++)
    Vertices.insert(std::pair<unsigned,vertex*>(it,new vertex(it)));
  int VertexIndex1(0), VertexIndex2(0);
  for(unsigned it=0;it<Matrix->size();++it){
    for(unsigned it2=0;it2<(*Matrix)[it].size();++it2){
      if((*Matrix)[it][it2]){
        if(!VertexIndex1) VertexIndex1=it2;
        else VertexIndex2=it2;
      }
  }
  Edges.push_back(new edge(it+1,Vertices[VertexIndex1+1],Vertices[VertexIndex2+1]));
  VertexIndex1=0;
  VertexIndex2=0;
  }
  EdgeCount=Edges.size();
  for(unsigned it=0;it<EdgeCount;it++){
    Edges[it]->Vertices.first->AddVertex(Edges[it]->Vertices.second);
    Edges[it]->Vertices.second->AddVertex(Edges[it]->Vertices.first);
  }
}

graph::graph(std::pair<int,TypeMatrix* > matrix):VerticeCount(matrix.second->size()){
  if(matrix.first==AdjacencyMatrix or matrix.first==AdjacencyList) InitiateByAdjacencyMatrix(matrix.second);
  else if(matrix.first==IncidenceMatrix){
    VerticeCount = matrix.second[0][0].size();
    InitiateByIncidenceMatrix(matrix.second);
  }
  //qDebug() << PrintConnectedVertices().c_str();
  //qDebug() << PrintEgdes().c_str();
  delete matrix.second;
}

graph::~graph(){
  std::vector<edge*>::iterator it=Edges.begin();
  for(;it!=Edges.end();++it)
    delete (*it);
  std::map<unsigned,vertex*>::iterator it2=Vertices.begin();
  for(;it2!=Vertices.end();++it2)
    delete (*it2).second;
  Vertices.clear();
  Edges.clear();
}

std::string graph::PrintAdjacencyMatrix()const{
  std::stringstream STREAM;
  STREAM << "Adjacency Matrix representation\n";
  std::vector< std::vector<bool> > AdjacencyMatrix;
  std::map<unsigned,vertex*>::const_iterator it=Vertices.begin();
  for(;it!=Vertices.end();++it){
    std::vector<bool> Adjacency(VerticeCount,0);
    std::map<unsigned,vertex*>::const_iterator it2=(*it).second->ConnectedVertices.begin();
    for(;it2!=(*it).second->ConnectedVertices.end();++it2)
      Adjacency[*((*it2).second)-1]=true;
    AdjacencyMatrix.push_back(Adjacency);
  }
  for(unsigned it=0;it<VerticeCount;++it){
    for(unsigned it2=0;it2<VerticeCount;++it2)
      STREAM << AdjacencyMatrix[it][it2] << ' ';
    STREAM << '\n';
  }
  return STREAM.str();
}

std::string graph::PrintAdjacencyList()const{
  std::stringstream STREAM;
  STREAM << "Adjacency List representation\n";
  std::map<unsigned,vertex*>::const_iterator it=Vertices.begin();
  for(;it!=Vertices.end();++it){
    STREAM << (*it).first << ": ";
    std::map<unsigned,vertex*>::const_iterator it2=(*it).second->ConnectedVertices.begin();
    for(;it2!=(*it).second->ConnectedVertices.end();++it2)
      STREAM << *((*it2).second) << "->";
    STREAM << "\n";
  }
  return STREAM.str();
}

std::string graph::PrintIncidenceMatrix()const{
  std::stringstream STREAM;
  STREAM << "Incidence Matrix representation\n";
  std::vector< std::vector<short> > IncidenceMatrix;
  for(unsigned it=0;it<EdgeCount;++it){
  std::vector<short> Incidence(VerticeCount,0);
    Incidence[*(Edges[it]->Vertices.first)-1]=1;
    Incidence[*(Edges[it]->Vertices.second)-1]=-1;
    IncidenceMatrix.push_back(Incidence);
  }
  for(unsigned it=0;it<EdgeCount;++it){
    for(unsigned it2=0;it2<VerticeCount;++it2)
      STREAM << IncidenceMatrix[it][it2] << ' ';
    STREAM << '\n';
  }
  return STREAM.str();
}

std::string graph::PrintEgdes()const{
  std::stringstream STREAM;
  STREAM << "Edges\n";
  std::vector<edge*>::const_iterator it=Edges.begin();
  for(;it!=Edges.end();++it)
    STREAM << "Edge index: "<< (*it)->Index << "  [" << *((*it)->Vertices.first)<<','<<*((*it)->Vertices.second)<<"]\n";
  STREAM << '\n';
  return STREAM.str();
}

std::string graph::PrintConnectedVertices()const{
  std::stringstream STREAM;
  STREAM << "Vertices correlation\n";
  std::map<unsigned,vertex*>::const_iterator it=Vertices.begin();
  for(;it!=Vertices.end();++it)
    STREAM << (*it).second->PrintConnectedVertices();
  return STREAM.str();
}
void graph::SaveGraph(std::string FILENAME)const{
  if(std::ifstream(FILENAME.c_str())){
    //Ask if you want to overwrite
    //no? exit function
    //yes? continue function
  }
  std::ofstream FILE(FILENAME.c_str());
  if(FILE.is_open()){
    FILE << PrintAdjacencyMatrix() << '\n' << PrintAdjacencyList() << '\n' << PrintIncidenceMatrix();
    FILE.close();
    //show some window with "successfully saved";
  }
}

void graph::ResetColors()const{
  std::map<unsigned,vertex*>::const_iterator it=Vertices.begin();
  for(;it!=Vertices.end();++it)
    (*it).second->Colored=false;
}  

void graph::VisitVertex(vertex* CurrentVertex,std::vector<unsigned>& PossibleComponent)const{
  CurrentVertex->Color();
  std::map<unsigned,vertex*>::iterator it=CurrentVertex->ConnectedVertices.begin();
  PossibleComponent.push_back(*CurrentVertex);
  for(;it!=CurrentVertex->ConnectedVertices.end();++it)
    if(!((*it).second->GetColor()))
      VisitVertex((*it).second,PossibleComponent);
}

std::vector<unsigned> graph::FindBiggestConnectedComponent()const{  
  std::map<unsigned,vertex*>::const_iterator it=Vertices.begin();
  std::vector < std::vector <unsigned> >BiggestConnectedComponent;
  for(;it!=Vertices.end();++it){
    if(!((*it).second->GetColor())){
      BiggestConnectedComponent.push_back(std::vector<unsigned>());
      VisitVertex((*it).second,BiggestConnectedComponent.back());
    }
  }
  
  ResetColors();
  std::pair<unsigned,unsigned> Max;
  for(unsigned it2=0;it2<BiggestConnectedComponent.size();++it2){
    if(BiggestConnectedComponent[it2].size()>Max.first){
      Max.first=BiggestConnectedComponent[it2].size();
      Max.second=it2;
    }
  }
  return BiggestConnectedComponent[Max.second];
}


bool graph::NotInVector(std::vector<unsigned>& Vector,unsigned Value)const{
  std::vector<unsigned>::const_iterator it=Vector.begin();
  for(;it!=Vector.end();++it) if(*it==Value) return false;
  return true; 
}
  
void graph::CheckPath(unsigned CurrentVertex, vertex* NextVertex,std::vector<unsigned>& AlreadyColored,std::vector < std::vector <unsigned> >& HamiltonianCycle)const{
   AlreadyColored.push_back(*NextVertex);
   if(AlreadyColored.size()==VerticeCount and NextVertex->ConnectedVertices.find(CurrentVertex)!=NextVertex->ConnectedVertices.end()){
     HamiltonianCycle.push_back(AlreadyColored);
   }
   std::map<unsigned,vertex*>::const_iterator it=NextVertex->ConnectedVertices.begin();
   for(;it!=NextVertex->ConnectedVertices.end();++it){
     if(NotInVector(AlreadyColored,(*it).first)){
       std::vector<unsigned>NewAlreadyColored(AlreadyColored);
       CheckPath(CurrentVertex,(*it).second,NewAlreadyColored,HamiltonianCycle);
     }
   }
}

std::vector<unsigned>  graph::FindHamiltonianCycle(vertex* CurrentVertex)const{
  if(!CurrentVertex)CurrentVertex=(*Vertices.begin()).second;
  std::vector<unsigned> BiggestConnectedComponent = FindBiggestConnectedComponent();
  if(BiggestConnectedComponent.size()!=GetVerticeCount()){
      throw std::runtime_error("Graf nie jest spójny");
  }
  std::vector < std::vector <unsigned> >HamiltonianCycle;
  std::map<unsigned,vertex*>::const_iterator it=CurrentVertex->ConnectedVertices.begin();
  for(;it!=CurrentVertex->ConnectedVertices.end();++it){
    std::vector <unsigned> AlreadyColored;
    AlreadyColored.push_back(*CurrentVertex);
    CheckPath(*CurrentVertex,(*it).second,AlreadyColored,HamiltonianCycle);
  }
  if(HamiltonianCycle.size()==0) HamiltonianCycle.push_back(std::vector<unsigned>());
  return HamiltonianCycle.front();
}

std::vector<unsigned> graph::FindEulerCycle(){

    if(!CheckIfGraphPerformEulerCycleConditions()){
        throw std::runtime_error("Graf nie spełnia warunków.");
    }

    std::stack<unsigned> Stack;
    std::vector<unsigned> Cycle;
    std::vector<edgeHelper> VisitedVerticesIndexes;

    unsigned StartIndex = 1;
    //Stack.push(StartIndex);
    vertex* firstVertex = Vertices.at(StartIndex);

    EulerRecursive(Stack,Cycle,VisitedVerticesIndexes,firstVertex);

    while(!Stack.empty()){
        Cycle.push_back(Stack.top());
        Stack.pop();
    }

    return Cycle;

}

void graph::EulerRecursive(std::stack<unsigned>& Stack,
                           std::vector<unsigned>& Cycle,
                           std::vector<edgeHelper>& VisitedVerticesIndexes,
                           const vertex* CurrentVertex,
                           bool backing
                    )const{

    unsigned CurrentVertexIndex = CurrentVertex->GetIndex();
    const std::map<unsigned,vertex*> ConnectedVerticesMap = CurrentVertex->GetConnectedVerticesMap();
    bool NotFound = true;
    if(!backing){
        Stack.push(CurrentVertexIndex);
    }
    for(std::map<unsigned,vertex*>::const_iterator it = ConnectedVerticesMap.begin();
        it != ConnectedVerticesMap.end();
        it++){

        unsigned itIndex = (*it).first;
        vertex* itVertex = (*it).second;
        //Stack.push(itIndex);

        bool isVertexFree = true;

        edgeHelper CurrentEdge(CurrentVertexIndex,itIndex);
        //qDebug() << "\nVisitedVerticesIndexes:";
        for(std::vector<edgeHelper>::iterator it2 = VisitedVerticesIndexes.begin(); it2 < VisitedVerticesIndexes.end(); it2++){
            if(edgeHelper::Equals(CurrentEdge,*it2)){
                isVertexFree = false;
            }
            //qDebug() << "(" << (*it2).IndexVertex1 << "," << (*it2).IndexVertex2 << ") ";
        }
        //qDebug() << "currentIndex: " << CurrentVertexIndex;
        //qDebug() << "itIndex: " << itIndex;

        std::stack<unsigned> tempStack = Stack;
        //qDebug() << "Stack";
        while(!tempStack.empty()){
            //qDebug() << tempStack.top() << " ";
            tempStack.pop();
        }
        if(isVertexFree){
            //qDebug() << "going...\n";
            NotFound = false;

            VisitedVerticesIndexes.push_back(CurrentEdge);
            EulerRecursive(Stack,Cycle,VisitedVerticesIndexes,itVertex);
            break;
        }
        else{
            //qDebug() << "not going...\n";
        }
    }
    if(!Stack.empty() && NotFound){
        unsigned tempIndex = Stack.top();
        Cycle.push_back(tempIndex);
        //qDebug() << "----> DODAJE DO CYKLU : " << tempIndex << " powrotny?: " << backing;
        Stack.pop();
        if(CurrentVertexIndex != tempIndex){
            EulerRecursive(Stack,Cycle,VisitedVerticesIndexes,Vertices.at(tempIndex),true);
        }
        else if(!Stack.empty()){
            tempIndex = Stack.top();
            EulerRecursive(Stack,Cycle,VisitedVerticesIndexes,Vertices.at(tempIndex),true);
        }
    }
}

bool graph::CheckEdge(edge& Edge)const{
    for(unsigned i=0;i<EdgeCount;i++){
        if((Edges[i]->GetVertices().first==Edge.GetVertices().first and Edges[i]->GetVertices().second==Edge.GetVertices().second)or(Edges[i]->GetVertices().first==Edge.GetVertices().second and Edges[i]->GetVertices().second==Edge.GetVertices().first)){
            return true;
        }
    }
    return false;
}

bool graph::CheckIfGraphPerformEulerCycleConditions() {

    std::vector<unsigned> BiggestConnectedComponent = FindBiggestConnectedComponent();
    if(BiggestConnectedComponent.size()!=GetVerticeCount()){
        return false;
    }

    for(std::map<unsigned,vertex*>::const_iterator it = Vertices.begin(); it != Vertices.end(); it++){
        if((*it).second->GetConnectedVerticesMap().size() % 2 != 0){
            return false;
        }
    }

    return true;
}

void graph::BuildEulerGraph(){

    if(Edges.size() < 2){
        throw std::runtime_error("Graf posiada za małą ilość krawędzi");
    }

    if(!CheckIfGraphPerformEulerCycleConditions()){
        throw std::runtime_error("Graf wejściowy musi być eulerowski");
    }

    //do{
        for(unsigned i = 0; i < (rand() % (Edges.size() * Edges.size()) + Edges.size() ); i++){
            int r1 = -1;
            int r2 = -1;
            do{
                r1 = rand() % Edges.size();
                r2 = rand() % Edges.size();
            }while(r1 == r2);


            ChangeEdges(Edges.at( r1 ) , Edges.at( r2 ) );
        }
    //}while(!CheckIfGraphPerformEulerCycleConditions());

}

bool graph::ChangeEdges(edge* e1, edge* e2){

    std::pair<vertex*,vertex*> e1Pair = e1->Vertices;
    vertex* e1v1 = e1Pair.first;
    vertex* e1v2 = e1Pair.second;

    std::pair<vertex*,vertex*> e2Pair = e2->Vertices;
    vertex* e2v1 = e2Pair.first;
    vertex* e2v2 = e2Pair.second;


    //2 edge musza byc niezalezne
    std::set<unsigned> testSet;
    testSet.insert(e1v1->GetIndex());
    testSet.insert(e1v2->GetIndex());
    testSet.insert(e2v1->GetIndex());
    testSet.insert(e2v2->GetIndex());

    if(testSet.size() != 4){
        return false;
    }

    //nowy edge1 = e1v1 e2v2
    //nowy edge2 = e2v1 e1v2


    //sprawdzenie czy nowy edge nie bedzie lezal na istniejacy edgu
    for(std::vector<edge*>::iterator it = Edges.begin(); it < Edges.end(); it++){
        std::pair<vertex*,vertex*> tempEdgePair = (*it)->Vertices;
        vertex* tempV1 = tempEdgePair.first;
        vertex* tempV2 = tempEdgePair.second;

        if(e1v2->GetIndex() == tempV1->GetIndex() && e2v1->GetIndex() == tempV2->GetIndex()){
            return false;
        }
        if(e1v2->GetIndex() == tempV2->GetIndex() && e2v1->GetIndex() == tempV1->GetIndex()){
            return false;
        }
        if(e1v1->GetIndex() == tempV1->GetIndex() && e2v2->GetIndex() == tempV2->GetIndex()){
            return false;
        }
        if(e1v1->GetIndex() == tempV2->GetIndex() && e2v2->GetIndex() == tempV1->GetIndex()){
            return false;
        }

    }

    //e1->SetFirstVertex();
    e1->SetSecondVertex(e2v2);

    //e2->SetFirstVertex();
    e2->SetSecondVertex(e1v2);


    //qDebug() << "Zmieniam krawedzie " << e1->getIndex() << " i " << e2->getIndex();

    //dla e1v1
    e1v1->ConnectedVertices.erase(e1v1->ConnectedVertices.find(e1v2->GetIndex()));
    e1v1->ConnectedVertices.insert(std::pair<unsigned,vertex*>(e2v2->GetIndex(),e2v2));

    //dla e1v2
    e1v2->ConnectedVertices.erase(e1v2->ConnectedVertices.find(e1v1->GetIndex()));
    e1v2->ConnectedVertices.insert(std::pair<unsigned,vertex*>(e2v1->GetIndex(),e2v1));

    //dla e2v1
    e2v1->ConnectedVertices.erase(e2v1->ConnectedVertices.find(e2v2->GetIndex()));
    e2v1->ConnectedVertices.insert(std::pair<unsigned,vertex*>(e1v2->GetIndex(),e1v2));

    //dla e2v2
    e2v2->ConnectedVertices.erase(e2v2->ConnectedVertices.find(e2v1->GetIndex()));
    e2v2->ConnectedVertices.insert(std::pair<unsigned,vertex*>(e1v1->GetIndex(),e1v1));

    return true;
}


const edge* graph::EdgeBetween(const vertex* const FirstVertex,const vertex* const SecondVertex)const{
  if(FirstVertex==SecondVertex) return NULL;
  std::vector<edge*>::const_iterator it= Edges.begin();
  for(;it!=Edges.end();++it){
    if((*it)->Vertices.first==FirstVertex and (*it)->Vertices.second==SecondVertex) return *it;
    if((*it)->Vertices.first==SecondVertex and (*it)->Vertices.second==FirstVertex) return *it;
  }
  return NULL;

}
void graph::ColorAndAddLightest(std::vector<vertex*>& VerticesTree,std::vector<unsigned>&MinimumSpanningTree)const{
  if(!VerticesTree.size()==VerticeCount) return;
  int MimWeight=MAX_EDGE_WEIGHT+1;
  vertex* ClosestFrom=NULL;
  vertex* ClosestTo=NULL;
  unsigned itVT=0;
  for(;itVT!=VerticesTree.size();++itVT){
    std::map<unsigned,vertex*>::const_iterator it=VerticesTree[itVT]->ConnectedVertices.begin();
    for(;it!=VerticesTree[itVT]->ConnectedVertices.end();++it){
      if((EdgeBetween(VerticesTree[itVT],(*it).second))->GetWeight()<MimWeight and !((*it).second->GetColor())){
    ClosestTo=(*it).second;
    ClosestFrom=VerticesTree[itVT];
    MimWeight=(EdgeBetween(VerticesTree[itVT],(*it).second))->GetWeight();
      }
    }
  }
  if(ClosestTo){
    ClosestTo->Color();
    MinimumSpanningTree.push_back(EdgeBetween(ClosestFrom,ClosestTo)->getIndex());
    VerticesTree.push_back(ClosestTo);
  }
}

std::vector<unsigned>  graph::FindMinimumSpanningTree(vertex* CurrentVertex)const{
  if(!CurrentVertex)CurrentVertex=(*Vertices.begin()).second;
  std::vector<unsigned> BiggestConnectedComponent = FindBiggestConnectedComponent();
  if(BiggestConnectedComponent.size()!=GetVerticeCount()){
      throw std::runtime_error("Graf nie jest spójny");
  }
  std::vector<unsigned>MinimumSpanningTree; 
  CurrentVertex->Color();
  std::vector<vertex*>VerticesTree;
  VerticesTree.push_back(CurrentVertex);
  while(VerticesTree.size()!=VerticeCount) ColorAndAddLightest(VerticesTree,MinimumSpanningTree);
  ResetColors();
  return MinimumSpanningTree;
}

dijkstraResult* graph::DijkstraAlgorithm(unsigned StartIndex){
    //StartIndex--;
    unsigned n = GetVerticeCount() + 1;
    if(StartIndex >= n){
        throw std::runtime_error("Niedozwolony index początkowy");
    }

    //https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm
    std::vector<unsigned> Q;

    int UNDEFINED = -1;
    int* d = new int[n];//odleglosc
    int* p = new int[n];//index poprzednika

    for(unsigned i = 0; i < n; i++){
        d[i] = INT_MAX;
        p[i] = UNDEFINED;
    }

    d[StartIndex] = 0;

    for(std::map<unsigned,vertex*>::iterator it = Vertices.begin();it != Vertices.end();it++){
        Q.push_back((*it).second->GetIndex());
    }

    while(!Q.empty()){
        int min = INT_MAX;
        int u = -1;
        for(unsigned i = 0; i < n; i++) {
            if( std::find(Q.begin(),Q.end(),i) != Q.end() && ( d[i] < min) ) {
                min = d[i];
                u = i;
            }
        }
        if(u == -1){
            //qDebug() << "break u = -1";
            break;
        }

        //remove u from Q

        std::vector<unsigned>::iterator removeUfromQ = std::find(Q.begin(),Q.end(),u);
        //qDebug() << "Szukam: " << u;
        //qDebug() << "Usuwam: " << (*removeUfromQ);
        if(removeUfromQ != Q.end()){
            Q.erase( removeUfromQ);
        }
        else{
            //qDebug() << "u nie znalezione, error";
            break;
        }

        //qDebug() << "u: " << u;

        vertex* uVertex = NULL;
        for(std::map<unsigned,vertex*>::iterator it = Vertices.begin(); it != Vertices.end();it++){
            if((*it).second->GetIndex() == u){
                uVertex = (*it).second;
                break;
            }
        }

        if(uVertex == NULL){
            //qDebug() << "uVertex = NULL, break";
            break;
        }

        /*
           for each neighbor v of u:           // where v is still in Q.
              alt ← dist[u] + length(u, v)
              if alt < dist[v]:               // A shorter path to v has been found
                dist[v] ← alt
                 prev[v] ← u
         */
        //qDebug() << "Mapa sasiadow dla: " << uVertex->GetIndex();
        const std::map<unsigned,vertex*> connectedVerticesMap = uVertex->GetConnectedVerticesMap();
        for(std::map<unsigned,vertex*>::const_iterator it = connectedVerticesMap.begin();
            it != connectedVerticesMap.end();
            it++){
            vertex* v = (*it).second;
            //qDebug() << "Szukam dla: " << v->GetIndex();
            if(std::find(Q.begin(),Q.end(),v->GetIndex()) != Q.end()){
                //qDebug() << "Znalazlem dla: " << v->GetIndex();
                int length_u_v = 0;

                for(std::vector<edge*>::iterator it = Edges.begin(); it < Edges.end(); it++){
                    vertex* firstVertex = (*it)->GetFirstVertex();
                    vertex* secondVertex = (*it)->GetSecondVertex();
                    if((firstVertex->GetIndex()== u && secondVertex->GetIndex() == v->GetIndex())
                       || (firstVertex->GetIndex() == v->GetIndex() && secondVertex->GetIndex() == u) ){
                       length_u_v = (*it)->GetWeight();
                       break;
                    }
                }


                int alt = d[u] + length_u_v;
                if(alt < d[v->GetIndex()]){
                    d[v->GetIndex()] = alt;
                    p[v->GetIndex()] = u;
                    //qDebug() << "Ustawiam dla wierzcholka = " << v->GetIndex() << " odleglosc: " << alt << " length_u_v= " << length_u_v << "d[u] = " << d[u];
                }
                else{
                    //qDebug() << "Nie ustawiam dla wierzcholka = " << v->GetIndex();
                }
            }
            else{
                //qDebug() << "Nie znalazlem dla: " << v->GetIndex();
            }
        }










    }
    //qDebug() << "KONIEC ALGO";
    //qDebug() << "Q: [";
    for(std::vector<unsigned>::iterator it = Q.begin(); it < Q.end();it++){
        //qDebug() << *it << " ";
    }
    //qDebug() << "p: [";
    for(unsigned i = 0; i < n; i++){
        //qDebug() << "[" << i << "] " << p[i] << " ";
    }
    //qDebug() << "]";
    //qDebug() << "d: [";
    for(unsigned i = 0; i < n; i++){
        //qDebug() << "[" << i << "] " << d[i] << " ";
    }
    //qDebug() << "]";

    dijkstraResult* result = new dijkstraResult;
    result->distance = d;
    result->predecessorVertexIndex = p;
    result->startIndex = StartIndex;
    result->size = n;

    return result;
}

std::pair<int,TypeMatrix* > graph::CreateBiggestConnectedComponent(){
    std::vector<unsigned> Indexes=BiggestStronglyConnectedComponent();
    std::sort (Indexes.begin(), Indexes.end());
    std::vector<vertex*> VerticesOfConnectedComponent;
    for(std::vector<unsigned>::iterator i=Indexes.begin();i!=Indexes.end();i++){
        VerticesOfConnectedComponent.push_back(Vertices[(*i)]);
        //qDebug()<<(*i)<<"index";
    }
    TypeMatrix* Matrix= new TypeMatrix;
    MakeIncidenceEdge(Matrix,Indexes.size(),Indexes.size());
    int TypeOfRepresentation=AdjacencyMatrix;

    for(unsigned it=0;it<VerticesOfConnectedComponent.size();it++){
        //qDebug()<<"wierzcholek: "<<VerticesOfConnectedComponent[it]->GetIndex();
        for(unsigned it2=0;it2<VerticesOfConnectedComponent.size();it2++){
            if((VerticesOfConnectedComponent[it2])!=(VerticesOfConnectedComponent[it])){
                if(VerticesOfConnectedComponent[it]->GetConnectedVerticesMap().count(Indexes[it2])){
                    //qDebug()<<VerticesOfConnectedComponent[it]->GetConnectedVerticesMap().find(Indexes[it2])->first;
                    //qDebug()<<Indexes[it2];
                    //qDebug()<<it<<it2;
                    (*Matrix)[it][it2]=true;
                }
            }
        }
     }
    std::pair<int,TypeMatrix* > PairOfMatrixAndType=make_pair(TypeOfRepresentation,Matrix);
    return PairOfMatrixAndType;
}

/*unsigned** graph::MatrixOfDistance(){
    unsigned** Matrix=new unsigned*[VerticeCount];
    for(unsigned i=0;i<VerticeCount;i++){
        Matrix[i]=new unsigned[VerticeCount];
    }
    //qDebug()<<"MACIERZ ODLEGLOSCI:";
    for(unsigned j=0;j<VerticeCount;j++){
        dijkstraResult* Dijkstra=DijkstraAlgorithm(j+1);
        for(unsigned i=0;i<VerticeCount;i++){
            Matrix[j][i]=Dijkstra->distance[i+1];
        }
    }
    return Matrix;
}*/
TypeMatrixInt* graph::MatrixOfDistance(){
    TypeMatrixInt* Matrix=new TypeMatrixInt;
    //qDebug()<<"MACIERZ ODLEGLOSCI:";
    for(unsigned j=0;j<VerticeCount;j++){
        dijkstraResult* Dijkstra=DijkstraAlgorithm(j+1);
        std::vector<int> Row;
        (*Matrix).push_back(Row);
        for(unsigned i=0;i<VerticeCount;i++){
            (*Matrix)[j].push_back(Dijkstra->distance[i+1]);
        }
    }
    return Matrix;
}

unsigned graph::Center(){
    TypeMatrixInt* Matrix=MatrixOfDistance();
    unsigned* sum=new unsigned[VerticeCount];
    unsigned index;
    for(unsigned i=0;i<VerticeCount;i++)
        sum[i]=0;
    for(unsigned i=0;i<VerticeCount;i++){
        for(unsigned j=0;j<VerticeCount;j++){
            sum[i]+=(*Matrix)[j][i];
        }
    }
    unsigned smallest = INT_MAX;
    for (unsigned i = 0; i < VerticeCount; i++) {
        if (sum[i] < smallest) {
            smallest = sum[i];
            index=i+1;
        }
    }
    delete Matrix;
    delete[] sum;
    return index;
}

unsigned graph::CenterMinMax(){
    TypeMatrixInt* Matrix=MatrixOfDistance();
    int* max=new int[VerticeCount];
    unsigned index;
    for(unsigned i=0;i<VerticeCount;i++)
        max[i]=0;
    for(unsigned i=0;i<VerticeCount;i++){
        for(unsigned j=0;j<VerticeCount;j++){
            if((*Matrix)[j][i]>max[i])
                max[i]=(*Matrix)[j][i];
        }
    }
    int smallest = INT_MAX;
    for (unsigned i = 0; i < VerticeCount; i++) {
        if (max[i] < smallest) {
            smallest = max[i];
            index=i+1;
        }
    }
    delete Matrix;
    delete[] max;
    return index;
}

FIND_SSS_IN_ADJACENCY_LIST::FIND_SSS_IN_ADJACENCY_LIST(unsigned Vertice,std::list<unsigned> LIST[]):
    VerticeCount(Vertice),
    adj(new std::list<unsigned>[VerticeCount]),
    sss(new unsigned[VerticeCount]),
    sssF(new unsigned[VerticeCount]),
    sssvis(new bool[VerticeCount]),
    adjT(new std::list<unsigned>[VerticeCount])
{
    for(unsigned it=0;it!=VerticeCount;++it){
        std::list<unsigned>::iterator ite=LIST[it].begin();
        for(;ite!=LIST[it].end();++ite){
            adj[it].push_back(*ite);
            //qDebug() << (*ite) << "->" << adj[it].back() << "Size: " << adj[it].size() ;
            //qDebug() << "";
        }
    }
}

FIND_SSS_IN_ADJACENCY_LIST::~FIND_SSS_IN_ADJACENCY_LIST(){
    delete[] adj;
    delete[] sss;
    delete[] sssF;
    delete[] sssvis;
    delete[] adjT;
}

void FIND_SSS_IN_ADJACENCY_LIST::TransposeGraph() {
  for(unsigned i = 0; i < VerticeCount; ++i)
    for(std::list<unsigned>::iterator it = adj[i].begin(); it != adj[i].end(); ++it)
      adjT[*it].push_back(i);
}


void FIND_SSS_IN_ADJACENCY_LIST::SssDfs(unsigned v) {
    sssvis[v] = 1;
    for(std::list<unsigned>::iterator it=adj[v].begin(); it != adj[v].end(); ++it){
      if(!sssvis[*it])
          SssDfs(*it);
    }
    sssF[ttt++] = v;

}

void FIND_SSS_IN_ADJACENCY_LIST::SssDfs2(unsigned v) {
    sss[v] = ns;
    sssvis[v] = 1;
    for(std::list<unsigned>::iterator it=adjT[v].begin(); it != adjT[v].end(); ++it)
      if(!sssvis[*it]) SssDfs2(*it);
}

void FIND_SSS_IN_ADJACENCY_LIST::ComputeSss() {
    for(unsigned i = 0; i < VerticeCount; ++i) sssvis[i] = 0;
    ttt=0;
    for(unsigned i = 0; i < VerticeCount; ++i)
        if(!sssvis[i]) SssDfs(i);
    for(unsigned i = 0; i < VerticeCount; ++i)
      sssvis[i] = 0;
    ns = 0;
    for(int i = ttt-1; i >= 0; --i)
      if(!sssvis[sssF[i]]) {
        SssDfs2(sssF[i]);
        ++ns;
      }
}

std::vector < std::vector< unsigned> > FIND_SSS_IN_ADJACENCY_LIST:: Result(){
    TransposeGraph();
    ComputeSss();
    std::vector < std::vector< unsigned> > StronglyConnectedComponents;
    for(unsigned i = 0; i < ns; ++i) {
        std::vector<unsigned> StronglyConnectedComponent;
        for(unsigned v = 0; v < VerticeCount; ++v){
          if(sss[v] == i) StronglyConnectedComponent.push_back(v+1);
        }
        StronglyConnectedComponents.push_back(StronglyConnectedComponent);
        StronglyConnectedComponent.clear();
      }
    return StronglyConnectedComponents;
}

std::vector < std::vector<unsigned> > graph::StronglyConnectedComponent()const{
    std::map<unsigned,vertex*>::const_iterator it =Vertices.begin();
    std::list<unsigned> ListGraph [VerticeCount];
    for(;it!=Vertices.end();++it){
        std::map<unsigned,vertex*>::const_iterator cit =(*it).second->ConnectedVertices.begin();
        for(;cit!=(*it).second->ConnectedVertices.end();++cit){
           ListGraph[*((*it).second) -1].push_back(*((*cit).second)-1);
        }
    }
    FIND_SSS_IN_ADJACENCY_LIST StronglyConnectedComponents(VerticeCount,ListGraph);
    return StronglyConnectedComponents.Result();
}

std::vector <unsigned> graph::BiggestStronglyConnectedComponent()const{
    std::vector < std::vector<unsigned> > StronglyConnectedComponent=this->StronglyConnectedComponent();
    unsigned indexOfSizeMax;
    size_t sizeMax=0;
    for(unsigned i=0;i<StronglyConnectedComponent.size();i++){
        if(sizeMax<StronglyConnectedComponent[i].size()){
            sizeMax=StronglyConnectedComponent[i].size();
            indexOfSizeMax=i;
        }
    }
    return StronglyConnectedComponent[indexOfSizeMax];
}

void  graph::CheckCycle(vertex* CurrentVertex,std::vector<unsigned>& PossibleCycle,std::set< std::vector<unsigned>,COMPARE_CYCLE >& Cycles)const{
    std::map<unsigned,vertex*>::const_iterator it = CurrentVertex->ConnectedVertices.begin();
    for(;it!=CurrentVertex->ConnectedVertices.end();++it)
    {
        if(!(NotInVector(PossibleCycle,(*it).second->GetIndex()))){
            std::vector<unsigned> Cycle;
            std::vector<unsigned>::const_iterator it2=std::find(PossibleCycle.begin(),PossibleCycle.end(),(*it).second->GetIndex());
            for(;it2!=PossibleCycle.end();++it2) Cycle.push_back(*it2);
            Cycles.insert(Cycle);
        }
    }
}

void graph::VisitVertexDirected(vertex* CurrentVertex,std::vector<unsigned> PossibleCycle,std::set< std::vector<unsigned>,COMPARE_CYCLE >& Cycles,std::vector<unsigned>AlreadyVisited)const{
  AlreadyVisited.push_back(CurrentVertex->GetIndex());
  std::map<unsigned,vertex*>::iterator it=CurrentVertex->ConnectedVertices.begin();
  PossibleCycle.push_back(*CurrentVertex);
  for(;it!=CurrentVertex->ConnectedVertices.end();++it){
      if(NotInVector(AlreadyVisited,(*it).second->GetIndex()))
      VisitVertexDirected((*it).second,PossibleCycle,Cycles,AlreadyVisited);

  }
  CheckCycle(CurrentVertex,PossibleCycle,Cycles);
}

std::vector< std::vector<unsigned> > graph::Cycles(vertex* CurrentVertex)const{
    if(!CurrentVertex)CurrentVertex=(*Vertices.begin()).second;
    std::set< std::vector<unsigned>,COMPARE_CYCLE > Cycles;
    std::vector< std::vector<unsigned> > UniqueCycles;
    std::vector < std::vector <unsigned> >PossibleCycles;
    PossibleCycles.push_back(std::vector<unsigned>());
    VisitVertexDirected(CurrentVertex,PossibleCycles.back(),Cycles,std::vector<unsigned>());
    std::set< std::vector<unsigned> >::const_iterator it= Cycles.begin();
    for(;it!=Cycles.end();++it){
        UniqueCycles.push_back(*it);
        //qDebug() << "SIZE: " << UniqueCycles.back().size();
    }
    return UniqueCycles;
}

QString graph::TopologicSort(){
    TopologicSort::Graph g = TopologicSort::Graph(VerticeCount);


    for(std::vector<edge*>::iterator it = Edges.begin(); it < Edges.end();it++){
        edge* e = *it;
        qDebug() << "(" << e->GetFirstVertex()->GetIndex() - 1 << "," << e->GetSecondVertex()->GetIndex() - 1 << ")";
        g.addEdge(e->GetFirstVertex()->GetIndex() - 1,e->GetSecondVertex()->GetIndex() - 1);
    }

    return g.topologicalSort();
}

QString graph::JohnsonAlgorithm(){
        QString str = "";
        Bellman BelFord(this,1);
        BelFord.GetDistance();

        int cost[100][100], a[100][100], i, j, k, c;

        int n, m;
        //cout << "Enter no of vertices";
        //cin >> n;
        n = VerticeCount;
        //cout << "Enter no of edges";
        //cin >> m;
        m =  EdgeCount;
        //cout << "Enter the\nEDGE Cost\n";
        /*for (k = 1; k <= m; k++)
        {
            cin >> i >> j >> c;
            a[i][j] = cost[i][j] = c;
        }*/
        //std::vector<edge*> Edges
        for (i = 1; i <= n; i++){
            for (j = 1; j <= n; j++)
            {
                if (i != j){
                    a[i][j] = 31999;
                }
                else{
                    a[i][j] = 0;
                }
            }
        }
        for (std::vector<edge*>::iterator it = Edges.begin(); it < Edges.end();it++){
            edge* e = *it;
            vertex* v1 = e->GetFirstVertex();
            vertex* v2 = e->GetSecondVertex();
            i = v1->GetIndex();
            j = v2->GetIndex();
            c = e->GetWeight();
            a[i][j] = cost[i][j] = c;
            qDebug() << i << " " << j << " " << c;
        }
        //algo
        for (i = 1; i <= n; i++)
            for (j = 1; j <= n; j++)
            {
                if (a[i][j] == 0 && i != j){
                    a[i][j] = 31999;
                }
                qDebug() << "i:"<<i<<" j:"<<j<<" a[i][j]:"<<a[i][j];
            }
        for (k = 1; k <= n; k++)
            for (i = 1; i <= n; i++)
                for (j = 1; j <= n; j++)
                    a[i][j] = std::min(a[i][j], a[i][k] + a[k][j]);
        str.append("Rezultat\n");
        for (i = 1; i <= n; i++)
        {
            for (j = 1; j <= n; j++)
            {
                if (a[i][j] != 31999){
                    str.append( QString::number( a[i][j] ) ).append("   ");
                    if(a[i][j]<10){
                        str.append(" ");
                    }
                }
            }
            str.append("\n\n");
        }


    return str;
}

Bellman::Bellman(const graph* GRAPH,const unsigned indexOfSource):INF(1000000001){
    N = GRAPH->GetVerticeCount();
    M = GRAPH->GetEdgeCount();
    s = indexOfSource-1;
    std::vector<edge*> EDGES=GRAPH->GetEdges();
    for(int i=0; i<M; i++){
        u=EDGES[i]->GetFirstVertex()->GetIndex();
        v=EDGES[i]->GetSecondVertex()->GetIndex();
        w=EDGES[i]->GetWeight();
        Edges.push_back(EDGE(u-1, v-1, w));
    }
}

void Bellman::init(){ //inicjujemy wszystkie tablice
    for(int i=0; i<N; i++){
        d[i]=INF; //poczatkowo odleglosc kazdego wierzcholka v od zrodla s wynosi nieskoczonosc
        poprzednik[i]=-1; //nikt nie ma zadnych poprzednikow bo nie ma zadnych sciezek
    }
    d[s]=0; //odleglosc zrodla od niego samego wynosi oczywiscie 0
}

bool Bellman::bellman(){ //oblicza dlugosci oraz przebieg najkrotszych sciezek miedzy wierzcholkiem zrodlowym s a wszystkimi innymi wierzcholkami
    for(int i=1; i<N; i++){ //wykonujemy N-1 przebiegow (tyle przebiegow wystarczy zeby obliczyc wszystkie najkrotsze sciezki)
        for(int j=0; j<M; j++){ //dla kazdej krawedzi w grafie (w naszym przypadku poniewaz graf jest nieskierowany to krawedzi jest 2M,
            //bo kazdej krawedzi towarzysszy krawedz odwrotna) ...
            if(d[Edges[j].v] > d[Edges[j].u] + Edges[j].w){ //jezeli dlugosc sciezki s~>v jest wieksza niz s~>u + waga krawedzi u->v to...
                d[Edges[j].v] = d[Edges[j].u] + Edges[j].w; //... zaktualizuj optymalny winik dla v i...
                poprzednik[Edges[j].v]=Edges[j].u; //... ustaw u jako poprzednika v na aktualnej najkrotszej sciezce s~>v
            }
        }
    }
    for(int i=0; i<M; i++){ //sprawdzamy czy w grafie nie ma cyklu o ujemnej wadze, ktory powoduje ze odleglosc wierzcholka v nalezacego do tego cyklu mozna
        //dowolnie zmniejszyc przechodzac przez ten cykl wiele razy (bo cykl ma sume wag krawedzi mniejsza niz zero czyli dziala jak 'zagiecie przestrzeni i czasu' ;D)
        if(d[Edges[i].v] > d[Edges[i].u] + Edges[i].w)return false; //aby to sprawdzic nalezy sprawdzic czy mimo N-1 przebiegow algorytmu (ktore wedlug definicji MUSZA
        //wystarczyc do wyznaczenia minimalnych sciezek), wciaz da sie zmniejszyc odleglosc v od s
    }
    return true; //w grafie nie ma cyklu o ujemnej wadze wiec wyniki sa prawidlowe
}

void Bellman::droga(int v){ //odtwarza droge v~>s idac po poprzednikach na najkrotszej sciezce
    do{
       // std::cout<<v+1<<" ";
        v=poprzednik[v];
    }while(poprzednik[v]!=-1);
}

std::vector<int> Bellman::GetDistance(){
    std::vector<int> Distance;
    for(int i=0; i<N; i++)
        Distance.push_back(0);
    init();
    if(!bellman()){ //w grafie wystepuje cykl o ujemnej wadze
        throw std::runtime_error("W grafie wystepuje cykl o ujemnej wadze i nie da sie obliczyc najkrotszych sciezek.");
    }else{
        for(int i=0; i<N; i++){
            if(i!=s)
                Distance[i]=d[i];
        }
    }
    return Distance;
}

std::vector <int> graph::BellmanFordAlgorithm(const unsigned index)const{
    Bellman BelFord(this,index);
    return BelFord.GetDistance();
}

Floyd::Floyd(graph* GRAPH):INF(1000000001){
    N = GRAPH->GetVerticeCount();
    M = GRAPH->GetEdgeCount();
    init();
    std::vector<edge*> EDGES=GRAPH->GetEdges();
    for(int i=0; i<M; i++){
        u=EDGES[i]->GetFirstVertex()->GetIndex();
        v=EDGES[i]->GetSecondVertex()->GetIndex();
        w=EDGES[i]->GetWeight();
        G[u-1][v-1]=g[u-1][v-1]=w;
    }
    Bellman BelFord(GRAPH,1);
    BelFord.GetDistance();
}

void Floyd::init(){ //inicjuje graf i pozostale struktury wartosciami poczatkowymi
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            G[i][j]=INF;
            g[i][j]=INF;
            R[i][j]=-1;
        }
        G[i][i]=0;
        g[i][i]=0;
    }
}

QString Floyd::floyd(){ //znajduje najkrotsze sciezki miedzy kazda para wierzcholkow w grafie i wypelnia macierz kierowanie ruchem zeby mozna bylo odtworzyc przebieg kazdej drogi
    for(int k=0; k<N; k++){
        for(int i=0; i<N; i++){
            for(int j=0; j<N; j++){
                if(g[i][j]>g[i][k]+g[k][j]){ //jezeli droga z i do j, poprzez wierzcholek posredni k jest krotsza niz aktualnie najkrotsza znana trasa i->j to zaktualizuj
                    g[i][j]=g[i][k]+g[k][j];
                    R[i][j]=k; //oznacza to ze idac po sciezce i~>j trzeba przejsc przez k
                }
            }
        }
    }
    QString out="Odległości między poszczególnymi parami wierzchołków: \n";
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            out+=QString::number(i+1)+"->"+QString::number(j+1);
            if (g[i][j]<100000000)
                out+=" odległość: "+QString::number(g[i][j])+"\n";
            else
                out+=" ścieżka nie istnieje \n";
        }
    }
    return out;
}

void Floyd::droga(int u, int v){ //odtwarza najkrotsza sciezke miedzy danymi wierzcholkami wykorzystujac macierz kierowania ruchem
    if(R[u][v]!=-1){ //dopoki nie dojdziemy do zwyklej krawedzi po ktorej trzeba wejsc to zchodz rekurencyjnie i wypisuj wierzcholek posredni k
        droga(u, R[u][v]);
        //cout<<R[u][v]+1<<" ";
        droga(R[u][v], v);
    }
}

QString graph::FloydWarshallAlgorithm(){
   Floyd FLOYD(this);
   QString out=FLOYD.floyd();
   //return FLOYD.floyd();
   // QString out="test";
   return out;
}


