#include "TopologicSort.h"

TopologicSort::Graph::Graph(int V)
{
    this->V = V;
    adj = new list<int>[V];
}

void TopologicSort::Graph::addEdge(int v, int w)
{
    adj[v].push_back(w); // Add w to v’s list.
}

// A recursive function used by topologicalSort
void TopologicSort::Graph::topologicalSortUtil(int v, bool visited[],
                                stack<int> &Stack)
{
    // Mark the current node as visited.
    visited[v] = true;

    // Recur for all the vertices adjacent to this vertex
    list<int>::iterator i;
    for (i = adj[v].begin(); i != adj[v].end(); ++i)
        if (!visited[*i])
            topologicalSortUtil(*i, visited, Stack);

    // Push current vertex to stack which stores result
    qDebug() << "Push to stack " << v;
    Stack.push(v);
}

// The function to do Topological Sort. It uses recursive
// topologicalSortUtil()
QString TopologicSort::Graph::topologicalSort()
{
    stack<int> Stack;

    // Mark all the vertices as not visited
    bool *visited = new bool[V];
    for (int i = 0; i < V; i++)
        visited[i] = false;

    // Call the recursive helper function to store Topological
    // Sort starting from all vertices one by one
    for (int i = 0; i < V; i++)
      if (visited[i] == false)
        topologicalSortUtil(i, visited, Stack);

    // Print contents of stack
    QString str = "";
    while (Stack.empty() == false)
    {
        int value = Stack.top() + 1;
        str.append(QString::number(value)).append(" ");
        qDebug() << value << " ";
        Stack.pop();
    }
    return str;
}
