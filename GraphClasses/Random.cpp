#include "Random.h"
#include <stdexcept>
#include <cstdlib>
#include <sstream>
#include <algorithm>
#include <iterator>
#include "EnumTypeOfRepresentation.h"

void MakeIncidenceVertex(TypeMatrix* matrix, unsigned row,unsigned columns){
    row++;
    std::vector<bool> RowVector;
	for(unsigned j=0;j<columns;++j){
		RowVector.push_back(false);
	}
	(*matrix).push_back(RowVector);
}

void MakeIncidenceEdge(TypeMatrix* matrix,unsigned rows,unsigned columns){
	for (unsigned i=0;i<rows;++i){
		MakeIncidenceVertex(matrix,i,columns);
	}
}

int Newton( int n, int k)
{
	double result = 1;
 	for( int i = 1; i <= k; i++){
		result = result*(n-i+1)/i;
	}
	return result;
}


std::pair<int,TypeMatrix* > MakeRandomIncidence(int vertex, int edge){
    if(vertex<=0) throw std::runtime_error("Incorrect number of vertexes.");
    if(edge<=0) throw std::runtime_error("Incorrect number of edges.");
    if(edge>Newton(vertex,2)) throw std::runtime_error("Too many edges in relation to the vertexes. It is impossible to create this graph.");
	int TypeOfRepresentation=IncidenceMatrix;
	TypeMatrix* Matrix= new TypeMatrix;
	MakeIncidenceEdge(Matrix,edge,vertex);
	for(unsigned i=0;i<Matrix->size();i++){
		unsigned RandVertex = rand()%vertex;
		unsigned RandVertex2 = rand()%vertex;
		while(RandVertex==RandVertex2)RandVertex2=rand()%vertex;
        for(unsigned j=0;j<Matrix->size();){
            if((*Matrix)[j][RandVertex]==true && (*Matrix)[j][RandVertex2]==true ){
                RandVertex = rand()%vertex;
                RandVertex2 = rand()%vertex;
                while(RandVertex==RandVertex2)RandVertex2=rand()%vertex;
                j=0;
            }
            else j++;
        }
		(*Matrix)[i][RandVertex]=true;
		(*Matrix)[i][RandVertex2]=true;
    }
	return make_pair(TypeOfRepresentation,Matrix);
}

std::pair<int,TypeMatrix* > MakeRandomDirected(int vertex){
    if(vertex<=0) throw std::runtime_error("Incorrect number of vertexes.");
    TypeMatrix* Matrix= new TypeMatrix(vertex);
    int TypeOfRepresentation=AdjacencyMatrix;
    for(unsigned i=0;i<vertex;++i) (*Matrix)[i].resize(vertex,false);
    for(unsigned it=0;it<(*Matrix).size();++it){
        for(unsigned it2=0;it2<(*Matrix)[it].size();++it2){
            if(it!=it2) (*Matrix)[it][it2]=rand()%2;
        }
    }
    return make_pair(TypeOfRepresentation,Matrix);
}

std::pair<int,TypeMatrix* > MakeRandomIncidenceProbability(int vertex, float probability){
	if (probability<0 || probability>1)throw std::runtime_error("Bad value of probability. Use a value of the interval [0,1].");
	int TypeOfRepresentation=AdjacencyMatrix;
	TypeMatrix* Matrix= new TypeMatrix;
	MakeIncidenceEdge(Matrix,vertex,vertex);
	float r;
	for(unsigned it=0;it<Matrix->size();it++){
		for(unsigned it2=it+1;it2<(*Matrix)[it].size();++it2){
			r = ((float) rand() / (RAND_MAX)+1)-1;
			if(r<probability){
				(*Matrix)[it][it2]=true;
				(*Matrix)[it][it2]=true;
				(*Matrix)[it2][it]=true;
				(*Matrix)[it2][it]=true;
			}
		}
	}
    return make_pair(TypeOfRepresentation,Matrix);

}

std::string RandomSequenceString(std::string sequence){
    unsigned RandChar;
    unsigned RandChar2;
    for(int i=0;i<5;i++){
        RandChar=rand()%sequence.length();
        while(RandChar%2==1)
            RandChar=rand()%sequence.length();
        RandChar2=rand()%sequence.length();
        while(RandChar2%2==1 or RandChar2==RandChar)
            RandChar2=rand()%sequence.length();
        char temp=sequence[RandChar];
        sequence[RandChar]=sequence[RandChar2];
        sequence[RandChar2]=temp;
    }
    return sequence;
}

unsigned* CheckEdgesInRandSequenceGraph(graph* TempGraph,unsigned i,unsigned* RandTab){
    unsigned RandIndexEdge=rand()%TempGraph->GetEdgeCount();
    unsigned RandIndexEdge2=rand()%TempGraph->GetEdgeCount();

    vertex* vertex11=TempGraph->GetEdges()[RandIndexEdge]->GetVertices().first;
    vertex* vertex12=TempGraph->GetEdges()[RandIndexEdge]->GetVertices().second;
    vertex* vertex21=TempGraph->GetEdges()[RandIndexEdge2]->GetVertices().first;
    vertex* vertex22=TempGraph->GetEdges()[RandIndexEdge2]->GetVertices().second;

    for(;i<TempGraph->GetEdgeCount();i++){
        RandIndexEdge=rand()%TempGraph->GetEdgeCount();
        vertex11=TempGraph->GetEdges()[RandIndexEdge]->GetVertices().first;
        vertex12=TempGraph->GetEdges()[RandIndexEdge]->GetVertices().second;
        while(TempGraph->GetEdges()[RandIndexEdge]->GetItWasRandomFirst()){
            RandIndexEdge=rand()%TempGraph->GetEdgeCount();
            vertex11=TempGraph->GetEdges()[RandIndexEdge]->GetVertices().first;
            vertex12=TempGraph->GetEdges()[RandIndexEdge]->GetVertices().second;
        }
        TempGraph->GetEdges()[RandIndexEdge]->SetItWasRandomFirst(true);
        for(unsigned j=0;j<(TempGraph->GetEdgeCount()-1-i);j++){
            RandIndexEdge2=rand()%TempGraph->GetEdgeCount();
            vertex21=TempGraph->GetEdges()[RandIndexEdge2]->GetVertices().first;
            vertex22=TempGraph->GetEdges()[RandIndexEdge2]->GetVertices().second;
            while(TempGraph->GetEdges()[RandIndexEdge2]->GetItWasRandomFirst() or TempGraph->GetEdges()[RandIndexEdge2]->GetItWasRandomSecond()){
                RandIndexEdge2=rand()%TempGraph->GetEdgeCount();
                vertex21=TempGraph->GetEdges()[RandIndexEdge2]->GetVertices().first;
                vertex22=TempGraph->GetEdges()[RandIndexEdge2]->GetVertices().second;
            }
            TempGraph->GetEdges()[RandIndexEdge2]->SetItWasRandomSecond(true);
            if(RandIndexEdge!=RandIndexEdge2 and vertex11->GetIndex()!=vertex21->GetIndex() and vertex11->GetIndex()!=vertex22->GetIndex()and vertex12->GetIndex()!=vertex21->GetIndex()and vertex12->GetIndex()!=vertex22->GetIndex()){
                vertex* vertex11=TempGraph->GetEdges()[RandIndexEdge]->GetVertices().first;
                vertex* vertex12=TempGraph->GetEdges()[RandIndexEdge]->GetVertices().second;
                vertex* vertex21=TempGraph->GetEdges()[RandIndexEdge2]->GetVertices().first;
                vertex* vertex22=TempGraph->GetEdges()[RandIndexEdge2]->GetVertices().second;

                edge TempEdge1(1,vertex11,vertex22);
                edge TempEdge2(2,vertex21,vertex12);
                if (!TempGraph->CheckEdge(TempEdge1) and !TempGraph->CheckEdge(TempEdge2)){
                    for(unsigned m=0;m<TempGraph->GetEdgeCount();m++){
                        TempGraph->GetEdges()[m]->SetItWasRandomSecond(false);
                    }
                    i++;
                    RandTab[0]=RandIndexEdge;
                    RandTab[1]=RandIndexEdge2;
                    RandTab[2]=i;
                    return RandTab;
                }else{
                    edge TempEdge1(1,vertex12,vertex22);
                    edge TempEdge2(2,vertex21,vertex11);
                    if (!TempGraph->CheckEdge(TempEdge1) and !TempGraph->CheckEdge(TempEdge2)){
                        i++;
                        RandTab[0]=RandIndexEdge;
                        RandTab[1]=RandIndexEdge2;
                        RandTab[2]=i;
                        return RandTab;
                    }
                }
            }
        }
        for(unsigned k=0;k<TempGraph->GetEdgeCount();k++){
            TempGraph->GetEdges()[k]->SetItWasRandomSecond(false);
        }
    }
    for(unsigned l=0;l<TempGraph->GetEdgeCount();l++){
        TempGraph->GetEdges()[l]->SetItWasRandomFirst(false);
    }
    for(unsigned m=0;m<TempGraph->GetEdgeCount();m++){
        TempGraph->GetEdges()[m]->SetItWasRandomSecond(false);
    }
    delete[] RandTab;
    delete TempGraph;
    throw std::runtime_error("Random change of this graph is impossible.");
}

graph* RandomSequenceGraph(std::string sequence){
    graph* TempGraph;
    TempGraph=GraphicSequence::makeGraphFromString(RandomSequenceString(sequence));
    unsigned it=0;
    unsigned* RandTab=new unsigned[3];
        while(it<TempGraph->GetEdgeCount()){
            RandTab=CheckEdgesInRandSequenceGraph(TempGraph,it,RandTab);
            it=RandTab[2];
            edge* Edge1=TempGraph->GetEdges()[RandTab[0]];
            edge* Edge2=TempGraph->GetEdges()[RandTab[1]];
            vertex* vertex11=TempGraph->GetEdges()[RandTab[0]]->GetVertices().first;
            vertex* vertex12=TempGraph->GetEdges()[RandTab[0]]->GetVertices().second;
            vertex* vertex21=TempGraph->GetEdges()[RandTab[1]]->GetVertices().first;
            vertex* vertex22=TempGraph->GetEdges()[RandTab[1]]->GetVertices().second;

            edge TempEdge1(1,vertex11,vertex22);
            edge TempEdge2(2,vertex21,vertex12);
            if (!TempGraph->CheckEdge(TempEdge1) and !TempGraph->CheckEdge(TempEdge2)){//zamiana drugiego wierzcholka z drugim
                vertex* TempVertex=Edge1->GetVertices().second;
                TempVertex->EraseConnectedVertex(Edge1->GetVertices().first->GetIndex());
                Edge1->GetVertices().first->EraseConnectedVertex(Edge1->GetVertices().second->GetIndex());
                Edge2->GetVertices().second->EraseConnectedVertex(Edge2->GetVertices().first->GetIndex());
                Edge2->GetVertices().first->EraseConnectedVertex(Edge2->GetVertices().second->GetIndex());

                Edge1->SetSecondVertex(Edge2->GetVertices().second);
                Edge2->SetSecondVertex(TempVertex);
                for(unsigned l=0;l<TempGraph->GetEdgeCount();l++){
                    TempGraph->GetEdges()[l]->SetItWasRandomFirst(false);
                }
                break;

            }else{//zamiana pierwszego wierzcholka z drugim
                edge TempEdge1(1,vertex12,vertex22);
                edge TempEdge2(2,vertex21,vertex11);
                    if (!TempGraph->CheckEdge(TempEdge1) and !TempGraph->CheckEdge(TempEdge2)){
                        vertex* TempVertex=Edge1->GetVertices().first;
                        TempVertex->EraseConnectedVertex(Edge1->GetVertices().second->GetIndex());
                        Edge1->GetVertices().second->EraseConnectedVertex(Edge1->GetVertices().first->GetIndex());
                        Edge2->GetVertices().second->EraseConnectedVertex(Edge2->GetVertices().first->GetIndex());
                        Edge2->GetVertices().first->EraseConnectedVertex(Edge2->GetVertices().second->GetIndex());

                        Edge1->SetFirstVertex(Edge2->GetVertices().second);
                        Edge2->SetSecondVertex(TempVertex);
                        for(unsigned l=0;l<TempGraph->GetEdgeCount();l++){
                            TempGraph->GetEdges()[l]->SetItWasRandomFirst(false);
                        }
                        break;
                    }
            }
        }
        for(unsigned l=0;l<TempGraph->GetEdgeCount();l++){
            TempGraph->GetEdges()[l]->SetItWasRandomFirst(false);
        }
    delete[] RandTab;
    return TempGraph;
}

graph* RandRegularGraph(unsigned kRegular){
    if(kRegular>24)
        throw std::runtime_error("Zła ilość sąsiadów wierzchołka (użyj liczby mniejszej od 25)");
    unsigned VertexCount;
    std::stringstream ss;
    do{
        ss.str("");
        VertexCount=kRegular+1+rand()%(25-kRegular);
        qDebug()<<VertexCount;
        for(unsigned i=0;i<VertexCount;i++)
            ss<<kRegular<<',';
    }while(!GraphicSequence::checkIfStringIsGraphicSequence(ss.str()));
    return GraphicSequence::makeGraphFromString(ss.str());
}


graph* RandomConnectedComponent(){
    int VertexesCount=3+rand()%8;
    //qDebug () << "VertexesCount" << VertexesCount;
    graph* GRAPH=new graph(MakeRandomDirected(VertexesCount));
    std::pair<int,TypeMatrix* > Matrix=GRAPH->CreateBiggestConnectedComponent();
    delete GRAPH;
    return new graph(Matrix);
}

graph* RandomAcycle(int n , int p){
    bool** g = new bool*[n];
    for(int i = 0; i < n; i++){
        g[i] = new bool[n];
        for(int j = 0; j < n; j++){
            if(i < j && rand() % 100 <= p){
                g[i][j] = true;
            }
            else{
                g[i][j] = false;
            }
        }
    }

    std::vector< std::vector<bool> >*  vg = new std::vector< std::vector<bool> >();

    for(int i = 0; i < n; i++){
        std::vector<bool> temp;
        for(int j = 0; j < n; j++){
            temp.push_back(g[i][j]);
        }
        vg->push_back(temp);
    }

    graph* newGraph = new graph(std::pair<int,TypeMatrix*>(AdjacencyMatrix,vg));
    return newGraph;
}
