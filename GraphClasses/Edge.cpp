#include "Edge.h"
#include <stdexcept>
#include <string>
#include <stdlib.h>
edge::edge(unsigned edgeindex,vertex* N1, vertex* N2):Index(edgeindex),Vertices(N1,N2),Weight((rand()%(MAX_EDGE_WEIGHT+6))-5),ItWasRandomFirst(false),ItWasRandomSecond(false){
  if(!N1 or !N2)
      throw std::runtime_error("Null pointer in 'edge' constructor\nterminated.\n");
}

void edge::SetFirstVertex(vertex* Vertex){
    Vertex->InsertConnectedVertices(Vertices.second->GetIndex(),Vertices.second);
    Vertices.second->InsertConnectedVertices(Vertex->GetIndex(),Vertex);
    Vertices.first=Vertex;
}

void edge::SetSecondVertex(vertex* Vertex){
    Vertex->InsertConnectedVertices(Vertices.first->GetIndex(),Vertices.first);
    Vertices.first->InsertConnectedVertices(Vertex->GetIndex(),Vertex);
    Vertices.second=Vertex;
}

bool edge::CheckIfIncludesVertex(unsigned Index)const{
    if(*(Vertices.first)==Index or *(Vertices.second)==Index) return true;
    return false;
}
