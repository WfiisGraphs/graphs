#include "FileOperations.h"
#include <stdexcept>
#include <iostream>
void CheckFile(std::ifstream& FILE){ 
  if(!FILE.is_open() or !FILE.good()) throw std::runtime_error("Cannot open file.\nCheck if file exists and you have access to open it and try again\n");
}

void PrintHelp(){
  std::cout << "\
Usage: ./graph FILE [-options]\n\
         (to execute graph program)\n\
FILE is text file with graph representation\n\
where options include:\n\
  -AD          text includes graph represented\n \
	       by Adjacency Matrix\n\
  -AL          text includes graph represented\n \
               by Adjacency Matrix\n\
  -IM          text includes graph represented\n \
	       by Adjacency Matrix\n \
		 " << std::endl;
}