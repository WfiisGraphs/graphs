#pragma once
#include "Edge.h"
#include "Vertex.h"
#include <vector>
#include <map>
#include <stack>
#include <set>
#include <stdexcept>
#include <math.h>
#include <QDebug>
#include <climits>
#include <QString>

typedef std::vector< std::vector<bool> > TypeMatrix;
typedef std::vector< std::vector<int> > TypeMatrixInt;

struct edgeHelper{
    unsigned IndexVertex1;
    unsigned IndexVertex2;

    edgeHelper(unsigned i1, unsigned i2): IndexVertex1(i1),IndexVertex2(i2){

    }

    static bool Equals(edgeHelper& edgeLeft,edgeHelper& edgeRight){
        if( (edgeLeft.IndexVertex1 == edgeRight.IndexVertex1 &&
             edgeLeft.IndexVertex2 == edgeRight.IndexVertex2) ||
             (edgeLeft.IndexVertex1 == edgeRight.IndexVertex2 &&
             edgeLeft.IndexVertex2 == edgeRight.IndexVertex1)
                ){
            return true;
        }
        return false;
    }
};

/* Warning predecessor[0],distance[0] not used! */
struct dijkstraResult{
    int startIndex;
    int* predecessorVertexIndex;
    int* distance;
    int size;
    dijkstraResult(){
        startIndex = -1;
        size = -1;
        predecessorVertexIndex = NULL;
        distance = NULL;
    }

    ~dijkstraResult(){
        if(predecessorVertexIndex != NULL){
            delete [] predecessorVertexIndex;
        }
        if(distance != NULL){
            delete [] distance;
        }
    }
};

class FIND_SSS_IN_ADJACENCY_LIST {
public:
    FIND_SSS_IN_ADJACENCY_LIST(unsigned Vertice,std::list<unsigned> LIST[]);
    ~FIND_SSS_IN_ADJACENCY_LIST();
    std::vector < std::vector< unsigned> > Result();
private:
    void TransposeGraph();
    void SssDfs(unsigned v);
    void SssDfs2(unsigned v);
    void ComputeSss();
    unsigned VerticeCount;
    std::list<unsigned>* adj;
    unsigned *sss;
    unsigned ns;
    unsigned ttt;
    unsigned *sssF;
    bool *sssvis;
    std::list<unsigned> *adjT;
};

struct COMPARE_CYCLE{
  bool operator()(const std::vector <unsigned >& FIRST, const std::vector <unsigned >& SECOND) const{
      std::vector <unsigned > SORTEDFIRST(FIRST);
      std::vector <unsigned > SORTEDSECOND(SECOND);
      std::sort(SORTEDFIRST.begin(),SORTEDFIRST.end());
      std::sort(SORTEDSECOND.begin(),SORTEDSECOND.end());
      return SORTEDFIRST<SORTEDSECOND;
  }
};




class graph{
public:
  graph(std::pair<int,TypeMatrix* > matrix);
  ~graph();
  std::string PrintAdjacencyMatrix()const;
  std::string PrintAdjacencyList()const;
  std::string PrintIncidenceMatrix()const;
  std::string PrintEgdes()const;
  std::string PrintConnectedVertices()const;
  std::vector<unsigned> FindBiggestConnectedComponent()const;
  std::vector<unsigned> FindHamiltonianCycle(vertex* CurrentVertex=NULL)const;
  std::vector<unsigned> FindMinimumSpanningTree(vertex* CurrentVertex=NULL)const;
  unsigned GetVerticeCount()const{return VerticeCount;}
  unsigned GetEdgeCount()const{return EdgeCount;}
  const std::map<unsigned,vertex*> GetVerticesMap()const{return Vertices;};
  void SetEdgeWeight(unsigned index,int Weight){Edges[index-1]->Weight=Weight;}
  const std::vector<edge*>GetEdges()const{return Edges;}
  const edge* EdgeBetween(const vertex* const,const vertex* const)const;
  void SaveGraph(std::string)const;
  void BuildEulerGraph();
  bool CheckIfGraphPerformEulerCycleConditions();
  std::vector<unsigned> FindEulerCycle();
  bool CheckEdge(edge&)const;
  bool ChangeEdges(edge* e1, edge* e2);
  std::vector < std::vector<unsigned> > StronglyConnectedComponent()const;
  std::vector <unsigned> BiggestStronglyConnectedComponent()const;
  std::vector < std::vector<unsigned> > Cycles(vertex* CurrentVertex=NULL)const;
  dijkstraResult* DijkstraAlgorithm(unsigned StartIndex);
  std::pair<int,TypeMatrix* > CreateBiggestConnectedComponent();
  TypeMatrixInt* MatrixOfDistance();
  unsigned Center();
  unsigned CenterMinMax();
  QString TopologicSort();
  QString JohnsonAlgorithm();
  std::vector <int> BellmanFordAlgorithm(const unsigned)const;
  QString FloydWarshallAlgorithm();
private: 
  std::map<unsigned,vertex*>Vertices;
  std::vector<edge*> Edges;
  unsigned VerticeCount;
  unsigned EdgeCount;
  void InitiateByAdjacencyMatrix(TypeMatrix*);
  void InitiateByIncidenceMatrix(TypeMatrix*);
  void VisitVertex(vertex* CurrentVertex,std::vector<unsigned>&)const;
  void CheckPath(unsigned CurrentVertex,vertex* NextVertex,std::vector<unsigned>& AlreadyColored,std::vector < std::vector <unsigned> >& HamiltonianCycle)const;
  void CheckCycle(vertex* CurrentVertex,std::vector<unsigned>& PossibleCycle,std::set< std::vector<unsigned>,COMPARE_CYCLE >& Cycles)const;
  void VisitVertexDirected(vertex* CurrentVertex,std::vector<unsigned> PossibleCycle,std::set< std::vector<unsigned>,COMPARE_CYCLE >&,std::vector<unsigned>)const;
  void ResetColors()const;
  bool NotInVector(std::vector<unsigned>& Vector,unsigned Value)const;
  void ColorAndAddLightest(std::vector<vertex*>&,std::vector<unsigned>&)const;
  void EulerRecursive(std::stack<unsigned>& Stack,
                      std::vector<unsigned>& Cycle,
                      std::vector<edgeHelper>& VisitedVerticesIndexes,
                      const vertex* CurrentVertex,
                      bool backing = false
                      )const;
};

struct Bellman{
    Bellman(const graph* GRAPH,const unsigned indexOfSource);
    int N, M, u, v, w; //N-ilosc wierzcholkow; M-ilosc krawedzi
    const int INF; //nieskonczonosc
    struct EDGE{ //struktura reprezentujaca krawedz. Taka reprezentacja grafu jest wygodna w algorytmie Bellmana-Forda
        int u;
        int v;
        int w;
        EDGE(int i, int j, int k){
            u=i;
            v=j;
            w=k;
        }
    };
    std::vector<EDGE> Edges; //struktura przechowujaca krawedzie
    int d[10000]; //struktura przechowujaca dlugosci (d[v]) najkrotszych drog laczacych wierzcholek s z v
    int poprzednik[10000]; //przechowuje poprzednik wierzcholka v na sciezce (najkrotszej) do s
    int s; //wierzcholek dla ktorego liczymy odleglosci (zrodlo) i do ktorego konstruujemy drogi
    void init();
    bool bellman();
    void droga(int v);
    std::vector<int> GetDistance();
};

struct Floyd{
    Floyd(graph* GRAPH);

    int G[1000][1000]; //dlugosci krawedzi G[u][v];
    int g[1000][1000]; //dlugosc najkrotszej sciezki u~>v
    int R[100][100]; //macierz kierowania ruchem
    const int INF;
    int N, M, u, v, w; //N-ilosc wierzcholkow; M-ilosc krawedzi

    void init();
    QString floyd();
    void droga(int u, int v);
};

