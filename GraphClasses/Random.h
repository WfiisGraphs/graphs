#include <vector>
#include <utility>
#include "GraphicSequence.h"


typedef std::vector<std::vector<bool> > TypeMatrix;
void MakeIncidenceVertex(TypeMatrix* matrix, unsigned row,unsigned columns);
void MakeIncidenceEdge(TypeMatrix* matrix,unsigned rows,unsigned columns);
int Newton( int n, int k);
std::pair<int,TypeMatrix* > MakeRandomIncidence(int vertex, int edge);
std::pair<int,TypeMatrix* > MakeRandomIncidenceProbability(int vertex, float probability);
unsigned* CheckEdgesInRandSequenceGraph(graph* TempGraph,unsigned i,unsigned* RandTab);
std::string RandomSequenceString(std::string sequence);
graph* RandomSequenceGraph(std::string sequence);
graph* RandRegularGraph(unsigned kRegular);
graph* RandomConnectedComponent();
graph* RandomAcycle(int n , int p);
