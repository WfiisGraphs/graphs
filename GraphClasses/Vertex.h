#pragma once
#include <map>
#include <sstream>

class vertex{
public:
  friend class graph;
  vertex(unsigned vertexindex);
  vertex(const vertex&);
  operator unsigned()const{return Index;}
  vertex& AddVertex(vertex*& othervertex);
  unsigned GetIndex()const{return Index;}
  std::string PrintConnectedVertices()const; 
  const std::map<unsigned,vertex*> GetConnectedVerticesMap()const{return ConnectedVertices;}
  void InsertConnectedVertices(unsigned Index,vertex* Vertex){ConnectedVertices.insert(std::pair<unsigned,vertex*>(Index,Vertex));}
  void EraseConnectedVertex(unsigned index){ConnectedVertices.erase(index);}
private:
  mutable bool Colored;
  unsigned Index;
  std::map<unsigned,vertex*> ConnectedVertices;
  void Color(){Colored=true;}
  bool GetColor()const{return Colored;}
  // Add data and rebuild to template
};
