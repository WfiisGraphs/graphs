#ifndef GRAPHICSEQUENCE_H
#define GRAPHICSEQUENCE_H

#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <stdexcept>
#include <utility>
#include "Graph.h"
#include "EnumTypeOfRepresentation.h"

class GraphicSequence
{
    static std::string debug;
    static std::vector<int>& makeIntVectorFromSequence(std::string string);
public:
    static std::string getDebug(){return debug;}
    static bool checkIfStringIsGraphicSequence(std::string string);
    static graph* makeGraphFromString(std::string string);
};

struct PairIntWrapper{
    PairIntWrapper(int _id , int _value):id(_id),value(_value){}
    int id;
    int value;
};


#endif // GRAPHICSEQUENCE_H
