#pragma once
#include "Vertex.h"
#define MAX_EDGE_WEIGHT 20
class edge{
public:
  friend class graph;
  edge(unsigned edgeindex,vertex* N1, vertex* N2);
  const std::pair<vertex*,vertex*> GetVertices()const{return Vertices;} 
  unsigned getIndex() const {return Index;}
  int GetWeight()const{return Weight;}
  void SetFirstVertex(vertex*);
  void SetSecondVertex(vertex*);
  bool GetItWasRandomFirst(){return ItWasRandomFirst;}
  bool GetItWasRandomSecond(){return ItWasRandomSecond;}
  void SetItWasRandomFirst(bool value){ItWasRandomFirst=value;}
  void SetItWasRandomSecond(bool value){ItWasRandomSecond=value;}
  vertex* GetFirstVertex(){ return Vertices.first; }
  vertex* GetSecondVertex(){ return Vertices.second; }
  bool CheckIfIncludesVertex(unsigned Index)const;
private:
  unsigned Index;
  std::pair<vertex*,vertex*> Vertices;
  int Weight;
  bool ItWasRandomFirst;
  bool ItWasRandomSecond;
};
