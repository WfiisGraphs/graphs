#ifndef TOPOLOGICSORT_H
#define TOPOLOGICSORT_H

#include<iostream>
#include <list>
#include <stack>
#include <QString>
#include <QDebug>
using namespace std;

namespace TopologicSort{

    class Graph
    {
        int V;    // No. of vertices'

        // Pointer to an array containing adjacency listsList
        list<int> *adj;

        // A function used by topologicalSort
        void topologicalSortUtil(int v, bool visited[], stack<int> &Stack);
    public:
        Graph(int V);   // Constructor

         // function to add an edge to graph
        void addEdge(int v, int w);

        // prints a Topological Sort of the complete graph
        QString topologicalSort();
    };



}


#endif // TOPOLOGICSORT_H
