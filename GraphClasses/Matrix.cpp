#include "Matrix.h"
#include <stdexcept>
#include <cctype>
#include <utility>
#include <QDebug>
#include "EnumTypeOfRepresentation.h"
void InitializeMatrix(TypeMatrix* Matrix, std::ifstream& FILE){
  std::string CurrentLine;
  while(getline(FILE,CurrentLine)){
    if(!CurrentLine.empty())
      Matrix->push_back(std::vector<bool>());
  }
  for(unsigned it=0;it!=Matrix->size();++it){
    for(unsigned it2=0;it2<Matrix->size();++it2)
      (*Matrix)[it].push_back(false);
  }
  FILE.clear();
  FILE.seekg(0);
}
  

void ConvertToBoolMatrix(TypeMatrix* Matrix, std::string LINE){
  std::string::const_iterator LINEIterator=LINE.begin();
  std::vector<bool> InsertVector;
  for(;LINEIterator!=LINE.end();++LINEIterator){
    if(*LINEIterator!=' '){ 
      bool BoolValue=(*LINEIterator=='1');
      InsertVector.push_back(BoolValue);
    }
  }  
  Matrix->push_back(InsertVector);
}

void ConvertToAdjacencyList(TypeMatrix* Matrix, std::string LINE){
  bool IsFirst=true;
  unsigned row=0;
  for(unsigned it=0;it<LINE.size();++it){
    if(isdigit(LINE[it])){
      if(IsFirst){
        row=LINE[it] -'0';
        IsFirst=false;
      }
      else{
      unsigned Vertex=LINE[it] -'0';
      (*Matrix)[row-1][Vertex-1]=true;
      }
    }
  }
}

void ConvertDirectedIncidenceMatrixToAdjacencyMatrix(TypeMatrix* Matrix, std::ifstream& FILE){
    std::vector< std::vector<short> > DigitalIncidenceMatrix;
    std::string LINE;
    while(getline(FILE,LINE)){
      if(!LINE.empty()){
          std::vector<short> InsertVector;
          std::string::const_iterator LINEIterator=LINE.begin();
          for(;LINEIterator!=LINE.end();++LINEIterator){
            if(*LINEIterator!=' '){
                if(*LINEIterator=='1') InsertVector.push_back(1);
                else if(*LINEIterator=='0') InsertVector.push_back(0);
                else if(*LINEIterator=='-'){
                    InsertVector.push_back(-1);
                    LINEIterator++;
                }
            }
          }
          DigitalIncidenceMatrix.push_back(InsertVector);
      }
    }
    Matrix->resize(DigitalIncidenceMatrix[0].size());
    for(unsigned resize=0;resize<Matrix->size();++resize) (*Matrix)[resize].resize(Matrix->size(),false);
    //qDebug() << "MATRIX: " << (*Matrix)[0][0];
    for(unsigned it=0;it<DigitalIncidenceMatrix.size();++it){
        unsigned First=-2;
        unsigned Second=-2;
        for(unsigned it2=0;it2<DigitalIncidenceMatrix[it].size();++it2){
            if(DigitalIncidenceMatrix[it][it2]==1) First=it2;
            if(DigitalIncidenceMatrix[it][it2]==-1) Second=it2;
        }
        //qDebug() << First << " " << Second;
        (*Matrix)[First][Second]=true;
    }
}

void FillMatrix(std::ifstream& FILE, TypeMatrix* Matrix,void (*Converter)(TypeMatrix*,std::string LINE)){
  std::string CurrentLine;
  while(getline(FILE,CurrentLine)){
    if(!CurrentLine.empty())
      Converter(Matrix,CurrentLine);
  }
}

void CheckCorrectness(TypeMatrix* Matrix){
  if(Matrix->size()==0)throw std::runtime_error("Cannot find any Matrix in file\nterminated.\n");
}

std::pair<int,TypeMatrix* > ConvertToMatrix(std::ifstream& FILE, int TypeOfRepresentation){
  TypeMatrix* Matrix=new TypeMatrix();
  if(TypeOfRepresentation==AdjacencyMatrix /* or TypeOfRepresentation==IncidenceMatrix*/)
      FillMatrix(FILE,Matrix,ConvertToBoolMatrix);
  else if(TypeOfRepresentation==IncidenceMatrix){
      ConvertDirectedIncidenceMatrixToAdjacencyMatrix(Matrix,FILE);
      TypeOfRepresentation=AdjacencyList;
  }
  else if(TypeOfRepresentation==AdjacencyList){
      InitializeMatrix(Matrix,FILE);
      FillMatrix(FILE,Matrix,ConvertToAdjacencyList);
  }
  else throw std::runtime_error("Cannot convert this type of Representation\nterminate.\n");
  
  CheckCorrectness(Matrix);
  return make_pair(TypeOfRepresentation,Matrix);
}
