#pragma once
#include <fstream>
#include <string>
#include <vector>

typedef std::vector< std::vector<bool> > TypeMatrix;
void ConvertToAdjacencyMatrix(TypeMatrix* Matrix, std::string LINE);
void FillMatrix(std::ifstream& FILE, TypeMatrix* Matrix);
void CheckCorrectness(TypeMatrix* Matrix);
std::pair<int,TypeMatrix* > ConvertToMatrix(std::ifstream& FILE,int TypeOfRepresentation);
void InitializeMatrix(TypeMatrix*,std::ifstream&);
void ConvertDirectedIncidenceMatrixToAdjacencyMatrix(TypeMatrix*,std::ifstream&);
