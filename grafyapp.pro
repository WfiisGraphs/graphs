#-------------------------------------------------
#
# Project created by QtCreator 2016-02-25T17:02:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = grafyapp
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    GraphClasses/Edge.cpp \
    GraphClasses/FileOperations.cpp \
    GraphClasses/Graph.cpp \
    GraphClasses/Matrix.cpp \
    GraphClasses/Vertex.cpp \
    GraphClasses/Random.cpp \
    GraphClasses/GraphicSequence.cpp \
    GraphClasses/TopologicSort.cpp

HEADERS  += mainwindow.h \
    GraphClasses/Edge.h \
    GraphClasses/FileOperations.h \
    GraphClasses/Graph.h \
    GraphClasses/Matrix.h \
    GraphClasses/Vertex.h \
    draw_graph.h \
    graphdrawer.h \
    GraphClasses/Random.h \
    GraphClasses/EnumTypeOfRepresentation.h \
    GraphClasses/GraphicSequence.h \
    GraphClasses/TopologicSort.h

FORMS    += mainwindow.ui
