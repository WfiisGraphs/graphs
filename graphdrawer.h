#ifndef GRAPHDRAWER_H
#define GRAPHDRAWER_H

#include "GraphClasses/Graph.h"
#include "GraphClasses/Random.h"

#include <QPointF>
#include <QPainter>
#include <QtCore/qmath.h>

class GraphDrawer
{
public:




    GraphDrawer(QPainter* _QPainter,
                graph* _graph,
                double _r,
                double _a,
                double _startX,
                double _startY,
                bool _showIndexes,
                bool _showWeights,
                std::vector<unsigned>& _vertexesToColor,
                std::vector<unsigned>& _EdgesIndexesToColor,
                unsigned _EulerCycleActiveVertex):
                painter(_QPainter),
                activeGraph(_graph),
                r(_r),
                a(_a),
                startX(_startX),
                startY(_startY),
                showIndexes(_showIndexes),
                showWeights(_showWeights),
                EulerCycleActiveVertex(_EulerCycleActiveVertex)

    {
        vertexesToColor = _vertexesToColor;
        edgesIndexesToColor = _EdgesIndexesToColor;
        setPainter();
    }

    void drawPointByIndex(unsigned i){


        QPointF point = getPointFByVertexIndex(i);

        if(i == EulerCycleActiveVertex){
            setRedColor();
        }
        else if(std::find(vertexesToColor.begin(),vertexesToColor.end(),(unsigned)i) != vertexesToColor.end()){
            setBlueColor();
        }
        else{
            setGreenColor();
        }

        painter->drawEllipse(point, 5, 5);
        if(showIndexes){

            setPlainColor();

            setHeaderFont();
            painter->drawText(QPointF(point.x() - 10,point.y() - 10),QString::number(i));
        }

    }

    QPointF getPointFByVertexIndex(int i){

        double x = r * qCos(a * i) + startX;
        double y = r * qSin(a * i) + startY;

        return QPointF(x,y);
    }

    void drawLineBetweenVertexes(int i , int i2,unsigned edgeNumber){
        QPointF point1 = getPointFByVertexIndex(i);
        QPointF point2 = getPointFByVertexIndex(i2);

        if(std::find(edgesIndexesToColor.begin(),edgesIndexesToColor.end(),(unsigned)edgeNumber) != edgesIndexesToColor.end()){
            setBlueColor();
        }
        else{
            setPlainColor();
        }

        double c = 15;
        double alfa = 45;
        double a = c * qSin(alfa);
        double b = c * qCos(alfa);


        QPointF pointBetween1And2((point1.x() + point2.x()) / 2,(point1.y() + point2.y()) / 2);
        QPointF pointBetween1AndPointBetween1And2((point1.x() + pointBetween1And2.x()) / 2,(point1.y() + pointBetween1And2.y()) / 2);

        QPointF point3(pointBetween1AndPointBetween1And2.x() - a,pointBetween1AndPointBetween1And2.y() + b);
        QPointF point4(pointBetween1AndPointBetween1And2.x() - a,pointBetween1AndPointBetween1And2.y() - b);

        QLineF line = QLineF(point1,point2);

        //QPointF pt1 = line.pointAt(0.4);
        //QPointF pt2 = line.pointAt(0.6);

        QPointF ptMiddle = line.pointAt(0.4); // line is the line you got from the QGraphicsLineItem - the base line for your triangle
        QLineF lineAdd;
        lineAdd.setP1(ptMiddle); // Start from the middle
        lineAdd.setLength(13.0); // Length for half the width of the triangle
        lineAdd.setAngle(line.angle()); // Bring to same direction as line
        lineAdd.setAngle(lineAdd.angle() - 150.0);
        QPointF pt1 = lineAdd.p2(); //pt1 is now exactly 3 pixels along the base line of the triangle
        // Do the same thing in the other direction
        lineAdd.setAngle(lineAdd.angle() - 60.0);
        QPointF pt2 = lineAdd.p2(); //pt2 is now exactly 3 pixels along the base line, but in the other direction

        painter->drawLine( line );
        painter->drawLine( QLineF(ptMiddle,pt1) );
        painter->drawLine( QLineF(ptMiddle,pt2) );



      QString str = "";
        if(showIndexes){
            str.append(QString::number(edgeNumber));
        }
        if(showWeights){
            int weight = 0;
            std::vector<edge*> e = activeGraph->GetEdges();
            for(std::vector<edge*>::const_iterator it = e.begin(); it < e.end();it++){
                if((*it)->getIndex() == edgeNumber){
                    weight = (*it)->GetWeight();
                }
            }
            str.append("[").append(QString::number(weight)).append("]");
        }
        setEdgeFont();
        painter->drawText( ptMiddle ,str);
    }


    void drawGraph(){



        const std::vector<edge*> v = activeGraph->GetEdges();

        for(std::vector<edge*>::const_iterator it = v.begin(); it < v.end();it++){

             std::pair<vertex*,vertex*> p = (*it)->GetVertices();

             drawLineBetweenVertexes(p.first->GetIndex(),p.second->GetIndex(),(*it)->getIndex() );
        }

        const std::map<unsigned int , vertex* > vMap = activeGraph->GetVerticesMap();

        for(std::map<unsigned int, vertex*>::const_iterator it = vMap.begin(); it != vMap.end();it++){

            const std::map<unsigned,vertex*> vMap2 = it->second->GetConnectedVerticesMap();

            //for(std::map<unsigned int, vertex*>::const_iterator it2 = vMap2.begin(); it2 != vMap2.end();it2++){
            //    drawLineBetweenVertexes(it->first,it2->first,edgeNumber);
            //}

            drawPointByIndex(it->first);

        }


        //painter.drawEllipse(QPointF(startX,startY), 50, 50);


    }

private:

    void setPainter(){
        painter->setRenderHint(QPainter::Antialiasing, true);
        //painter->setPen(QPen(Qt::black, 3, Qt::SolidLine, Qt::RoundCap));
        //painter->setBrush(QBrush(Qt::black, Qt::SolidPattern));
    }

    void setHeaderFont(){

        QFont font = painter->font() ;

        font.setPixelSize(16);
        font.setBold(true);
        painter->setFont(font);

    }

    void setEdgeFont(){

        QFont font = painter->font() ;

        font.setPixelSize(12);
        font.setItalic(true);

        painter->setFont(font);

    }

    void setPlainColor(){
        painter->setBackground(QBrush(QColor("#FFFFFF")));
        painter->setPen(QPen(Qt::black, 1, Qt::SolidLine));

    }
    void setGreenColor(){
        painter->setBackground(QBrush(QColor("#32992C")));
        painter->setPen(QPen(QColor("#32992C"), 2, Qt::SolidLine));
    }

    void setRedColor(){
        painter->setBackground(QBrush(QColor("#FFFFFF")));
        painter->setPen(QPen(Qt::red, 3, Qt::SolidLine));
    }

    void setBlueColor(){
        painter->setBackground(QBrush(QColor("#FFFFFF")));
        painter->setPen(QPen(Qt::blue, 2, Qt::SolidLine));
    }




    QPainter* painter;
    graph* activeGraph;
    double r;
    double a;
    double startX;
    double startY;
    bool showIndexes;
    bool showWeights;
    std::vector<unsigned> vertexesToColor;
    std::vector<unsigned> edgesIndexesToColor;
    unsigned EulerCycleActiveVertex;
};

#endif // GRAPHDRAWER_H
